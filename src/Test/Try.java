package Test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class Try extends BaseTest {
	
	@Test(enabled =true)
	public void Scenario1() throws Exception
	{
		driver.navigate().to("https://oneafp-oabpm-dt2.pegacloud.io/prweb/PRServlet");
		driver.manage().window().maximize();
		
		List<WebElement> inputs = driver.findElements(By.tagName("input"));
		inputs.get(0).sendKeys("Srikanth");
		inputs.get(1).sendKeys("rules");
		
		List<WebElement> buttons = driver.findElements(By.tagName("button"));
		buttons.get(0).click();
	}
}
