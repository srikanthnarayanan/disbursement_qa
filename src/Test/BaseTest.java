package Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.zaproxy.*;
import org.zaproxy.clientapi.ant.ZapTask;

import Page.*;
import utility.*;


public class BaseTest implements Config {
	
static{
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
        System.setProperty("current.date.time", dateFormat.format(new Date()));
    }

	protected static WebDriver driver;
	public static String currFolder,currTest,screenshotfilepath,currIteration,newFolder,exitIter;
	
	public static ArrayList<String> arrayColumnValues = new ArrayList<String>();
	public static ArrayList<String> arrayRowValues = new ArrayList<String>();
	public static ArrayList<String> arrayRowValues1 = new ArrayList<String>();
	
	public static int High_Wait = 3000;
	public static int Med_Wait = 2000;
	public static int Low_Wait = 1000;
	
	
	//-------Instantiate all Pages Here
	
	public static ReusableLibraries Lib;
	public static PageLogin Login;
	public static Actor1 Actor1;
	public static selectPlan selectPlan;
	public static DistributionOptions DistributionOptions;
	public static AccountBalance AccountBalance;
	public static ConfirmStatus ConfirmStatus;
	public static WithdrawalOptions WithdrawalOptions;
	public static ReviewSubmit ReviewSubmit;
	public static TaxElections TaxElections;
	public static Reservists Reservists;
	public static Summary Summary;
	public static sponsor sponsor;
	
	@SuppressWarnings("deprecation")
	public static void setup() throws Exception
	{
		System.setProperty("webdriver.gecko.driver",parentFolder + "\\Driver\\geckodriver.exe");
		//System.setProperty("webdriver.chrome.driver",parentFolder + "\\Driver\\chromedriver.exe");
		//System.setProperty("webdriver.ie.driver",parentFolder + "\\Driver\\IEDriverServer.exe");
		
		screenshotfilepath = parentFolder + "\\Utility\\";
/*		
		ChromeOptions chromeOptions = new ChromeOptions(); 
		chromeOptions.addArguments("--ignore-certificate-errors"); 
		
		// Set proxy
		String proxyAddress = "localhost:8095"; 
		Proxy proxy = new Proxy(); 
		proxy.setHttpProxy(proxyAddress).setSslProxy(proxyAddress); 
		
		// Set Desired Capabilities 
		DesiredCapabilities capabilities = DesiredCapabilities.chrome(); 
		capabilities.setCapability(CapabilityType.PROXY, proxy); 
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
		capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS,true); 
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions); 
		
		String path = parentFolder + "\\ZAP_2.7.0";
		path = "cmd /c " + path +  "\\zap.bat";
		Runtime.getRuntime().exec("cmd.exe /c start C:\\Users\\t003320\\eclipse-workspace\\Disbursement_New\\ZAP_2.7.0\\zap.bat");
		//Runtime.getRuntime().exec("zap.bat");
	*/	
		
		//FirefoxOptions options = new FirefoxOptions().setLogLevel(Level.OFF);
		//DesiredCapabilities capabilities = options.addTo(DesiredCapabilities.firefox());
		//driver = new FirefoxDriver(capabilities);
		
		//DesiredCapabilities dc = new DesiredCapabilities();
		//dc.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
		//d = new FirefoxDriver(dc);
		
		
		driver = new FirefoxDriver();
	//	driver = new ChromeDriver();
	//	driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		Login = new PageLogin();
		Lib = new ReusableLibraries(driver);
		//Actor1 = new Actor1();
		selectPlan = new selectPlan();
		DistributionOptions = new DistributionOptions();	
		AccountBalance = new AccountBalance();
		ConfirmStatus = new ConfirmStatus();
		WithdrawalOptions = new WithdrawalOptions();
		ReviewSubmit = new ReviewSubmit();
		TaxElections = new TaxElections();
		Reservists = new Reservists();
		Summary = new Summary();
		sponsor = new sponsor();
		
		
	}
	
	public static void setupDriver() throws Exception
	{
		
	}
	
	@AfterClass
	public void teardown()
	{
		//htmlReports.reportEND();
		//driver.quit();
	}
	
	/*
	public static void main(String [] args) throws Exception
	{

	}
	*/

}
