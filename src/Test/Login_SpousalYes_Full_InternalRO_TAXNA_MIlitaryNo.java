package Test;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import testDataFactory.testData;
import utility.Enums;
import utility.Enums.*;
import utility.ReusableLibraries;
import utility.htmlReports;

public class Login_SpousalYes_Full_InternalRO_TAXNA_MIlitaryNo extends BaseTest {
	
	//@Test(enabled =true)
	public static void Scenario1() throws Exception
	{
		//setup();
		//htmlReports.reportSTART(currTest);
		htmlReports.ReportLogger =htmlReports.Report.startTest(currTest);

		htmlReports.reportINFO("Setup Completed as expected");
		htmlReports.reportINFO("Start of Actor 1 Action");
		

		
		Login.LoginasQA();
		//Login.LoginasDEV();
		//Login.confirm_Login();
		//Login.Login(SpousalConsent.Yes, RMD.No, WithDrawalType.Full, TAXWH.NA, Reservist.Yes);
		//Login.CreateCase(SpousalConsent.Yes, RMD.Yes, WithDrawalType.Full, WithDrawalOption.InternalRollOver, TAXWH.NA, Reservist.Yes);
		
		selectPlan.select_Plan_Id();
		
		DistributionOptions.selectSFS();
		
		//
			//ConfirmStatus.QDRO_Explanation();
			//ConfirmStatus.QDRO_Yes();
			ConfirmStatus.QDRO_No();
			
			//---------------------------------------------
			if(AccountBalance.getAccountBalance().compareTo(AccountBalance.getTotalBalancebySource()) == 0)
			{
				htmlReports.reportPASS("Account Balance Matches with the Total by Sources");
			}
			else
			{
				htmlReports.reportFAIL("Account Balance doesn't Matches with the Total by Sources");
			}
			AccountBalance.clickNext();
			ReusableLibraries.sync(High_Wait);
			//------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalOptions").equalsIgnoreCase("Full"))
			{
				WithdrawalOptions.clickFull();
			}
			else
			{	
				WithdrawalOptions.clickPartial();
			}
			ReusableLibraries.sync(High_Wait);
			if(driver.getPageSource().contains("Annuity"))
			{
				AccountBalance.clickNext();
			}
			
			//------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Rollover_Cash").equalsIgnoreCase("Cash"))
			{
				WithdrawalOptions.selectCash();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Rollover_Cash").equalsIgnoreCase("Rollover"))
			{
				WithdrawalOptions.selectRollOver();
			}
			//----------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("External"))
			{
			WithdrawalOptions.selectExternalRollOver();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("Internal"))
			{
			WithdrawalOptions.selectInternalRollOver();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("Check"))
			{
				WithdrawalOptions.selectCheck();	
				WithdrawalOptions.TaxWHScreens();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("ACH"))
			{
				WithdrawalOptions.selectACH();	
				WithdrawalOptions.TaxWHScreens();
			}
			//-------------------------------------------------------------
			ReusableLibraries.sync(High_Wait);
			ReviewSubmit.clickMilitaryYes();
//			/ReviewSubmit.clickMilitaryNo();
			ReviewSubmit.Submit_TC();
			//-----------------------------------------------------------------
			ReviewSubmit.verifySuccess();
			String CaseNo = ReviewSubmit.fetchCaseNo();
			
			System.out.println(CaseNo);	
			htmlReports.reportINFO("Case number Generated is " + CaseNo);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "CaseNo", CaseNo);
		/*
		String transstatus[][] = AccountBalance.getTransactionNos();
		
		for(int x1 = 2;x1<transstatus.length;x1++)
		{
			for(int y = 0;y<transstatus[0].length;y++)
			{
				System.out.println(transstatus[x1][y]);
			}
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "fileId", transstatus[x1][0]);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "tranStatus", transstatus[x1][1]);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "userId", transstatus[x1][2]);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "tradeDate", transstatus[x1][3]);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "seqNo", transstatus[x1][4]);
		}
		*/
		Login.Logout();
		
		//driver.close();
		//htmlReports.reportEND();
	}
	
	@Test(enabled = true)
	public void Run() throws Exception
	{
		currTest = "CreateDistribution";
		setup();
		htmlReports.reportSTART(currTest);
		
		for(int i=3; i<=testData.getRowCount_TestCase("CreateDistribution",currTest);i++)
		{
			currIteration = ""+i;
			Scenario1();
		}
		driver.close();
		htmlReports.reportEND();
	}
		

}
