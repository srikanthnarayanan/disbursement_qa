package Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import Page.PageLogin;
import Page.sponsor;
import Scenario.Participant;
import Scenario.Sponsor;
import testDataFactory.testData;
import utility.Enums.DistributionOption;
import utility.Enums.RMD;
import utility.Enums.Reservist;
import utility.Enums.SpousalConsent;
import utility.Enums.TAXWH;
import utility.Enums.WithDrawalOption;
import utility.Enums.WithDrawalType;
import utility.ReusableLibraries;
import utility.htmlReports;

public class TC01 extends BaseTest {
	
	
	
	/*********************************************Run********************************************************************
	 * 
	 * 
	 * 
	 * @throws Exception
	 * *********************************************Run********************************************************************
	 */
		@Test(enabled = true)
		public void Run() throws Exception
		{
	//		test();
			
			currTest = "CreateDistribution";
			setup();
			htmlReports.reportSTART(currTest);
			System.setProperty("currenttest",currTest);
			Scenario3();
			htmlReports.reportEND();
			
			driver.close();
	
		}
			
public void test() throws Exception
{
	
	List <WebElement> allButtons = driver.findElements(By.className("button"));
	for(WebElement button : allButtons)
	{
		if(button.getAttribute("innerHTML").contains("No"))
		{
			button.click();
		}
	}
	//ValidateDivertReason
	

	/*System.out.println(Reason);
	String[] reasons = Reason.split("\\r?\\n");
	for(String rsn : reasons)
	{
		System.out.println("Text is " + rsn);
	}*/
	
}
		/*********************************************Scenarios********************************************************************
		 * 
		 * 
		 * 
		 * @throws Exception
		 * *********************************************Scenario1********************************************************************
		 */
	public void Scenario1() throws Exception
	{
		//setup();
		//htmlReports.reportSTART(currTest);
		htmlReports.ReportLogger =htmlReports.Report.startTest(currTest);
	
		htmlReports.reportINFO("Setup Completed as expected");
		htmlReports.reportINFO("Start of Actor 1 Action");
		
		Login.LoginasQA();
		//Login.LoginasDEV();
		//Login.confirm_Login();
		
		selectPlan.select_Plan_Id();
		
		DistributionOptions.selectSFS();
		
		//
			//ConfirmStatus.QDRO_Explanation();
			//ConfirmStatus.QDRO_Yes();
			ConfirmStatus.QDRO_No();
			
			//---------------------------------------------
			if(AccountBalance.getAccountBalance().compareTo(AccountBalance.getTotalBalancebySource()) == 0)
			{
				htmlReports.reportPASS("Account Balance Matches with the Total by Sources");
			}
			else
			{
				htmlReports.reportFAIL("Account Balance doesn't Matches with the Total by Sources");
			}
			AccountBalance.clickNext();
			ReusableLibraries.sync(High_Wait);
			//------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalOptions").equalsIgnoreCase("Full"))
			{
				WithdrawalOptions.clickFull();
			}
			else
			{	
				WithdrawalOptions.clickPartial();
			}
			ReusableLibraries.sync(High_Wait);
			if(driver.getPageSource().contains("Annuity"))
			{
				AccountBalance.clickNext();
			}
			
			//------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Rollover_Cash").equalsIgnoreCase("Cash"))
			{
				WithdrawalOptions.selectCash();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Rollover_Cash").equalsIgnoreCase("Rollover"))
			{
				WithdrawalOptions.selectRollOver();
			}
			//----------------------------------------------------------
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("External"))
			{
			WithdrawalOptions.selectExternalRollOver();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("Internal"))
			{
			WithdrawalOptions.selectInternalRollOver();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("Check"))
			{
				WithdrawalOptions.selectCheck();	
				WithdrawalOptions.TaxWHScreens();
			}
			else if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "WithdrawalType").equalsIgnoreCase("ACH"))
			{
				WithdrawalOptions.selectACH();	
				WithdrawalOptions.TaxWHScreens();
			}
			//-------------------------------------------------------------
			ReusableLibraries.sync(High_Wait);
			ReviewSubmit.clickMilitaryYes();
//			/ReviewSubmit.clickMilitaryNo();
			ReviewSubmit.Submit_TC();
			//-----------------------------------------------------------------
			ReviewSubmit.verifySuccess();
			String CaseNo = ReviewSubmit.fetchCaseNo();
			
			System.out.println(CaseNo);	
			htmlReports.reportINFO("Case number Generated is " + CaseNo);
			testData.setstringcellvalue1("CreateDistribution", currTest, "Iteration", currIteration, "CaseNo", CaseNo);

		Login.Logout();
		
		//driver.close();
		//htmlReports.reportEND();
	}

	//* *********************************************Scenario2********************************************************************

	public void Scenario2() throws Exception
	{
		
		String Caseno = Participant.CreateCase(DistributionOption.SFS,SpousalConsent.No, RMD.No, WithDrawalType.Full, WithDrawalOption.InternalRollOver, TAXWH.NA, Reservist.Yes);

		//String Caseno = "D-5189"
		System.out.println("Case number is " + Caseno);
		Sponsor.ApproveCase(Caseno.trim());
		//htmlReports.reportEND();
		
	}
	
	
	//* *********************************************Scenario3********************************************************************
	
	public void Scenario3() throws Exception
	{
		testData tData = new testData();
		int rowcount = tData.findRowcount("CreateCase"); 
		for(int i = 1; i<=rowcount-3;i++)
		{
			currIteration = "" + i;
			//htmlReports.reportSTART("Scenario3_Iteration" + i);
			String Desc = testData.getstringcellvalue("CreateCase","CreateCase","Iteration",currIteration,"Description");
			htmlReports.startTest(Desc + " - iteration " + i);
			String Caseno = Participant.CreateCase_Excel(i);

			if(Caseno.contains("Fail"))
			{
				System.out.println("Case Failed, Reason received is " + Caseno);
			}
			else
			{
				System.out.println("Case number is " + Caseno);
				Sponsor.ApproveCase(Caseno.trim());
				
				String caseStatus = Participant.waituntil_PendingDivertDecision(Caseno);
				htmlReports.reportINFO(caseStatus);
				
				if(caseStatus.equalsIgnoreCase("PENDING-DISTRIBUTION"))
				{
					//--commenting the click of Begin
					ReusableLibraries.Click("xpath", "//button[@class='Secondary pzhc pzbutton']");
					
					Participant.CompleteTasks();
					//Participant.getTransactions();
				}
				Login.Logout();
				htmlReports.endTest();
				
				/*
				//Participant.waituntil_PendingDivertDecision(Caseno.trim());
				Login.Logout();
				//htmlReports.reportEND();
				 * */ 
			}
		}
	}
	
	/*********************************************End of Scenarios********************************************************************
	 * 
	 * 
	 * 
	 * @throws Exception
	 * *********************************************End of Scenarios********************************************************************
	 */

}
