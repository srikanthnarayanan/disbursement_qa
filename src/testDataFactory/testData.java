package testDataFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Test.BaseTest;
import utility.variableDeclarations;

public class testData extends BaseTest
{
	public static XSSFWorkbook wb;
	public static XSSFSheet sh;
	public static XSSFCell cell;
	//static String cellValue;
	
	public static String getstringcellvalue(String sheetName, String rowValue, String ColumnValue ) throws Exception
	{
		String cellValue = null;
// declaring array and getting all column headers for the requested sheet 
		FileInputStream path = new FileInputStream(excelpath);						
		wb = new XSSFWorkbook(path);
		sh=wb.getSheet(sheetName);
		int colVal = sh.getRow(0).getLastCellNum();
		for(int i=0;i<colVal;i++)		{
			arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
		}
		
// declaring array and getting all row values for the requested sheet and column name
		int rowCount = sh.getLastRowNum()+1;
		int tc = (arrayColumnValues.indexOf("TestCaseID"));
		for(int i=0;i<rowCount;i++)		{		
			arrayRowValues.add(sh.getRow(i).getCell(tc).getStringCellValue());				// refer "utility.VariableDeclarations" for array declaration / Initiation	
		}
		
		int rowNumber = (arrayRowValues.indexOf(rowValue));									// returns row number from array
		int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
				
// cellValue is fetched based on row and column index
		cell = sh.getRow(rowNumber).getCell(columnNumber);

// cellValue is converted from integer to String before returning		
		if(cell.getCellType() ==Cell.CELL_TYPE_STRING) 	{
			cellValue = cell.getStringCellValue();
		}
		else 
		{
		cellValue = String.valueOf((int)cell.getNumericCellValue());
		}
		path.close();
		arrayRowValues.clear();
		arrayColumnValues.clear();
		return cellValue;		
	}
	
	
	public static int findRowcount(String sheetName) throws IOException
	{
		FileInputStream path = new FileInputStream(excelpath);						
		wb = new XSSFWorkbook(path);
		sh=wb.getSheet(sheetName);
		return sh.getLastRowNum();
	}
	
	// Writing into excel	
		public static void testOutputFile(String output, int rowval, int colval) throws Exception
		{
			File path = new File("C:\\Workspace\\Navigator\\TestData\\GTP_Navigator_Output.xlsx"); 
			FileInputStream fis = new FileInputStream(path);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet sheet = wb.getSheetAt(0);
//			int lastrow = sheet.getLastRowNum();						// To get last row of sheet 2 "workfile"
//			Row newRow = sheet.createRow(lastrow+1);					// To create new row
//			Cell newCell = newRow.createCell(1);						// To create new cell @ newly created row 
//			newCell.setCellValue(output);								// To pass work file id to excel
			
			sheet.getRow(rowval).createCell(colval).setCellValue(output);
			
			/*
			 DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			//System.out.println(dateFormat.format(date)); //2014/08/06 15:59:48
			Cell dt = newRow.createCell(1);
			dt.setCellValue(date);
			*/
			
			
			FileOutputStream fos = new FileOutputStream(path);
			wb.write(fos);
	}
		
		public static void setstringcellvalue(String sheetName, String rowValue, String ColumnValue, String cellvalue ) throws Exception
		{
			//String cellValue = null;
	// declaring array and getting all column headers for the requested sheet 
			FileInputStream path = new FileInputStream(excelpath);						
			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			for(int i=0;i<colVal;i++)		{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}
			
	// declaring array and getting all row values for the requested sheet and column name
			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			int tc1 = (arrayColumnValues.indexOf("Iteration"));
			for(int i=0;i<rowCount;i++)		{		
				arrayRowValues.add(sh.getRow(i).getCell(tc).getStringCellValue());				// refer "utility.VariableDeclarations" for array declaration / Initiation	
			}
			for(int i=0;i<rowCount;i++)		{		
				arrayRowValues1.add(sh.getRow(i).getCell(tc).getStringCellValue());				// refer "utility.VariableDeclarations" for array declaration / Initiation	
			}
			
			int rowNumber = (arrayRowValues.indexOf(rowValue));									// returns row number from array
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));	
			

					
	// cellValue is fetched based on row and column index
			cell = sh.getRow(rowNumber).getCell(columnNumber);

	// cellValue is converted from integer to String before returning		
			cell.setCellValue(cellvalue);
			path.close();
			arrayRowValues.clear();
			arrayColumnValues.clear();
			//return cellValue;		
		}
	
	
		public static String getstringcellvalue1(String sheetName, String rowValue, String iterationno, String ColumnValue ) throws Exception
		{
			int rowNumber=0;
			String cellValue = null;
	// declaring array and getting all column headers for the requested sheet 
			FileInputStream path = new FileInputStream(excelpath);						
			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			for(int i=0;i<colVal;i++)		{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}
			
	// declaring array and getting all row values for the requested sheet and column name
			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			int tc1 = (arrayColumnValues.indexOf("Iteration"));
			for(int i=0;i<rowCount;i++)		{		
				if(sh.getRow(i).getCell(tc).getStringCellValue().equalsIgnoreCase(rowValue) && sh.getRow(i).getCell(tc1).getStringCellValue().equalsIgnoreCase(iterationno))
				{
					rowNumber = i;
					break;
				}
			}
			
			//int rowNumber = (arrayRowValues.indexOf(rowValue));									// returns row number from array
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
					
	// cellValue is fetched based on row and column index
			cell = sh.getRow(rowNumber).getCell(columnNumber);

	// cellValue is converted from integer to String before returning	
		try {	
			if(cell.getCellType() ==cell.CELL_TYPE_STRING) 	{
				cellValue = cell.getStringCellValue();
			}
			else 
			{
			cellValue = String.valueOf((int)cell.getNumericCellValue());
			}
		}
		catch(Exception e)
		{
			cellValue = "";
		}
			path.close();
			arrayRowValues.clear();
			arrayColumnValues.clear();
			return cellValue;		
		}
		
		
		public static void setstringcellvalue1(String sheetName, String rowValue, String iterationno, String ColumnValue, String cellval1) throws Exception
		{
			//String excelpath1 = "C:\\Users\\t003320\\eclipse-workspace\\InvestmentID\\TestData\\InvestmentID1.xlsx";
			int rowNumber=0;
			String cellValue = null;
			FileInputStream path = new FileInputStream(new File(excelpath));	

			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			for(int i=0;i<colVal;i++)		{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}

			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			int tc1 = (arrayColumnValues.indexOf("Iteration"));
			for(int i=0;i<rowCount;i++)		{		
				if(sh.getRow(i).getCell(tc).getStringCellValue().equalsIgnoreCase(rowValue) && sh.getRow(i).getCell(tc1).getStringCellValue().equalsIgnoreCase(iterationno))
				{
					rowNumber = i;
					break;
				}
			}
			
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
			cell = sh.getRow(rowNumber).createCell(columnNumber);
			//cell.setCellValue(cellval1);
			cell.setCellValue(cellval1.toString());
			//path.close();
			path.close();
			FileOutputStream fop = new FileOutputStream(new File(excelpath));
			wb.write(fop);	
			fop.close();
			
			arrayRowValues.clear();
			arrayColumnValues.clear();	
		}
	
		public static int getRowCount(String sheetName) throws Exception
		{
			int rowNumber=0;
			String cellValue = null;
			FileInputStream path = new FileInputStream(new File(excelpath));	

			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			
			return sh.getLastRowNum();
		}
		
		public static String getCellData(String sheetName, int rowno, int colno) throws Exception
		{
			int rowNumber=0;
			String cellValue = null;
			FileInputStream path = new FileInputStream(new File(excelpath));	

			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			return sh.getRow(rowno).getCell(colno).getStringCellValue();
			
		}
		
		
		//-----------------New Libraries------------------------------------------------------
		
		public static String getstringcellvalue(String sheetName, String TCName, String identifier, String identifierVal, String ColumnValue ) throws Exception
		{
			int rowNumber=0;
			String cellValue = null;
			
			FileInputStream path = new FileInputStream(excelpath);						
			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			
			for(int i=0;i<colVal;i++)		
			{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue().trim());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}
			
			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			int tc1 = (arrayColumnValues.indexOf(identifier));
			
			for(int i=0;i<rowCount;i++)		
			{		
				if(sh.getRow(i).getCell(tc).getStringCellValue().equalsIgnoreCase(TCName) && sh.getRow(i).getCell(tc1).getStringCellValue().equalsIgnoreCase(identifierVal))
				{
					rowNumber = i;
					break;
				}
			}
								
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));	
			cell = sh.getRow(rowNumber).getCell(columnNumber);

			try 
			{	
				if(cell.getCellType() ==cell.CELL_TYPE_STRING) 	
				{
					cellValue = cell.getStringCellValue();
				}
				else 
				{
				cellValue = String.valueOf((int)cell.getNumericCellValue());
				}
			}
			
			catch(Exception e)
			{
				cellValue = "";
			}
			
			path.close();
			arrayRowValues.clear();
			arrayColumnValues.clear();
			return cellValue;		
		}
		
		public static String getstringcellvalue(String filepath, String sheetName, int rownumber, String ColumnValue ) throws Exception
		{
			String cellValue = null;
	// declaring array and getting all column headers for the requested sheet 
			FileInputStream path = new FileInputStream(filepath);						
			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			for(int i=0;i<colVal;i++)		{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue().trim());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}
			
	// declaring array and getting all row values for the requested sheet and column name
			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			//for(int i=0;i<rowCount;i++)		{		
			//	arrayRowValues.add(sh.getRow(i).getCell(tc).getStringCellValue());				// refer "utility.VariableDeclarations" for array declaration / Initiation	
			//}
			
			//int rowNumber = (arrayRowValues.indexOf(rowValue));									// returns row number from array
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
					
	// cellValue is fetched based on row and column index
			cell = sh.getRow(rownumber).getCell(columnNumber);

	// cellValue is converted from integer to String before returning		
			if(cell.getCellType() ==cell.CELL_TYPE_STRING) 	{
				cellValue = cell.getStringCellValue();
			}
			else 
			{
			cellValue = String.valueOf((int)cell.getNumericCellValue());
			}
			path.close();
			arrayRowValues.clear();
			arrayColumnValues.clear();
			return cellValue;		
		}
		
		
		
		public static void setstringcellvalue1(String sheetName, String TCName, String identifier, String identifierVal, String ColumnValue, String cellval1) throws Exception
		{
			//String excelpath1 = "C:\\Users\\t003320\\eclipse-workspace\\InvestmentID\\TestData\\InvestmentID1.xlsx";
			int rowNumber=0;
			//String cellval1 = null;
			FileInputStream path = new FileInputStream(new File(excelpath));	

			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			for(int i=0;i<colVal;i++)		{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}

			int rowCount = sh.getLastRowNum()+1;
			int tc = (arrayColumnValues.indexOf("TestCaseID"));
			int tc1 = (arrayColumnValues.indexOf(identifier));
			for(int i=0;i<rowCount;i++)		{		
				if(sh.getRow(i).getCell(tc).getStringCellValue().equalsIgnoreCase(TCName) && sh.getRow(i).getCell(tc1).getStringCellValue().equalsIgnoreCase(identifierVal))
				{
					rowNumber = i;
					break;
				}
			}
			
			int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
			cell = sh.getRow(rowNumber).createCell(columnNumber);
			//cell.setCellValue(cellval1);
			cell.setCellValue(cellval1.toString());
			//path.close();
			path.close();
			FileOutputStream fop = new FileOutputStream(new File(excelpath));
			wb.write(fop);	
			fop.close();
			
			arrayRowValues.clear();
			arrayColumnValues.clear();	
		}
		
		public static int getRowCount_TestCase(String sheetName, String tcName) throws Exception
		{
			int rowcnt=0;
			String cellValue = null;
			FileInputStream path = new FileInputStream(new File(excelpath));	

			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			
			for(int i = 1; i<=sh.getLastRowNum();i++)
			{
				cell = sh.getRow(i).getCell(0);
				if(cell.getStringCellValue().equalsIgnoreCase(tcName))
				{
					rowcnt = rowcnt + 1;
				}
			}
			return rowcnt;
		}
}


