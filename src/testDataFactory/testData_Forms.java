package testDataFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utility.variableDeclarations;

public class testData_Forms extends variableDeclarations
{
	public static XSSFWorkbook wb;
	public static XSSFSheet sh;
	public static XSSFCell cell;
	//static String cellValue;	
	
 
	public static String getstringcellvalue(String formExcelpath, String sheetName, int rowValue, String ColumnValue ) throws Exception
	{				
		String cellValue = null;
// declaring array and getting all column headers for the requested sheet 
		FileInputStream path = new FileInputStream(formExcelpath);						
		wb = new XSSFWorkbook(path);
		sh=wb.getSheet(sheetName);
		int colVal = sh.getRow(0).getLastCellNum();
		//System.out.println("Total Number of Columns:"+colVal);
		for(int i=0;i<colVal;i++)		
		{
			arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
		}
		
// declaring array and getting all row values for the requested sheet and column name
		int rowCount = sh.getLastRowNum()+1;
		//System.out.println("Total Number of Rows:"+rowCount);
		int tc = (arrayColumnValues.indexOf("FormType"));
		for(int i=0;i<rowCount;i++)		{		
			arrayRowValues.add(sh.getRow(i).getCell(tc).getStringCellValue());				// refer "utility.VariableDeclarations" for array declaration / Initiation	
		}
		
		//int rowNumber = (arrayRowValues.indexOf(rowValue));									// returns row number from array
		int rowNumber = rowValue;									                           // returns row number from array
		int columnNumber = (arrayColumnValues.indexOf(ColumnValue));						// returns column number from array	
				
// cellValue is fetched based on row and column index
		cell = sh.getRow(rowNumber).getCell(columnNumber);

// cellValue is converted from integer to String before returning		
		if(cell.getCellType() ==cell.CELL_TYPE_STRING) 	{
			cellValue = cell.getStringCellValue();
		}
		else 
		{
		cellValue = String.valueOf((int)cell.getNumericCellValue());
		}
		path.close();
		arrayRowValues.clear();
		arrayColumnValues.clear();
		return cellValue;		
	}
	

	 public static int getrowcountValue(String formExcelpath,String sheetName) throws Exception
		{
			String cellValue = null;
	// declaring array and getting all column headers for the requested sheet 
			
			FileInputStream path = new FileInputStream(formExcelpath);						
			wb = new XSSFWorkbook(path);
			sh=wb.getSheet(sheetName);
			int colVal = sh.getRow(0).getLastCellNum();
			//System.out.println("Total Number of Columns:"+colVal);
			for(int i=0;i<colVal;i++)		
			{
				arrayColumnValues.add(sh.getRow(0).getCell(i).getStringCellValue());			// refer "utility.VariableDeclarations" for array declaration / Initiation
			}
			
	// declaring array and getting all row values for the requested sheet and column name
			int rowCount = sh.getLastRowNum()+1;
			//System.out.println("Total Number of Rows:"+rowCount);
			
			return rowCount;	
		}	
		
	
}


