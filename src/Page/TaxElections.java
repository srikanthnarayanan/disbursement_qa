package Page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class TaxElections extends BaseTest {

	
	public void TaxElection_Yes() throws InterruptedException
	{
		htmlReports.reportINFO("------- In Tax Election Screen----");
		ReusableLibraries.sync(High_Wait);
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Your outstanding loan" + "')]"));
		if(list.size() > 0)
		{
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
		ReusableLibraries.sync(Low_Wait);
		list = driver.findElements(By.xpath("//*[contains(text(),'" + "The Internal Revenue Service requires income tax withholding" + "')]"));
		if(list.size() > 0)
		{
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("xpath", "//*[@class='content-item content-field item-1 flex flex-row dataValueWrite']");
		ReusableLibraries.sync(High_Wait);
		
		driver.findElement(By.xpath("//input[@data-test-id='20180903034712072280970' and @id='FederalTaxHolding']")).clear();
		ReusableLibraries.Set("xpath", "//input[@data-test-id='20180903034712072280970' and @id='FederalTaxHolding']","20");
		ReusableLibraries.Set("xpath", "//input[@data-test-id='20180903034712072280970' and @id='StateTaxHolding']","10");
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		
		
	}
	
	public void TaxElection_No() throws Exception
	{
		htmlReports.reportINFO("------- In Tax Election Screen----");
		ReusableLibraries.sync(High_Wait);
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Your outstanding loan" + "')]"));
		if(list.size() > 0)
		{
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
		ReusableLibraries.sync(Low_Wait);
		list = driver.findElements(By.xpath("//*[contains(text(),'" + "The Internal Revenue Service requires income tax withholding" + "')]"));
		if(list.size() > 0)
		{
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("xpath", "//*[@class='content-item content-field item-2 flex flex-row dataValueWrite']");
	}
	
	public void TaxElection_NA() throws InterruptedException
	{
		htmlReports.reportINFO("------- In Tax Election Screen----");
		ReusableLibraries.sync(High_Wait);
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Tax withholding is not required for this distribution" + "')]"));
		if(list.size() > 0)
		{
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
	}
}
