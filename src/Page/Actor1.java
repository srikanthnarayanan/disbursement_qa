package Page;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.logs;
import org.openqa.selenium.JavascriptExecutor;

public class Actor1 extends BaseTest{
	public WebElement linkNew, linkAddFunds,buttonChooseFile,buttonNext; 
	
	
	public void Screen1() throws Exception
	{
		//driver.switchTo().defaultContent();
		ReusableLibraries.Click("linktext","New");
		ReusableLibraries.Click("linktext","Add Funds");
	
		ReusableLibraries.switchFrame();
		
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Set("id","$PpyWorkPage$ppyTemplateInputBox",testData.getstringcellvalue("Login",currTest,"Role","A1","filepath"));
				
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		
	}
	
	public void Screen2() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
	}
	
	public void Screen3() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("cssselector",".case_description");
		ReusableLibraries.Set("id", "RFPercentage", "1.2");
		ReusableLibraries.Set("id","ClosedToAllInvestorDate",testData.getstringcellvalue1("AddFunds", "TC01", "1", "relasedateOmni"));
		ReusableLibraries.Set("id","ClosedToNewInvestorDate",testData.getstringcellvalue1("AddFunds", "TC01", "1", "relasedateOmni"));
		
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
	}
	
	public void Screen4() throws Exception
	{
		int no_of_Pages = driver.findElements(By.xpath("//*[@id=\"grid-desktop-paginator\"]/tbody/tr/td[3]/nobr/a")).size();
		if(no_of_Pages == 0)
		{
		//	ReusableLibraries.Button_Click(driver,"div","title","Disclose Group 2 : Investment Data");
		//	ReusableLibraries.Button_Click(driver,"div","title","Disclose Group 3 : Investment Lineup(s)");
		//	ReusableLibraries.Button_Click(driver,"div","title","Disclose Group 4: Investment Option Summary Assignments");
			ReusableLibraries.Click("cssselector","div.layout:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
			ReusableLibraries.Click("cssselector","div.layout:nth-child(5) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
			ReusableLibraries.Click("cssselector","div.layout:nth-child(6) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
			ReusableLibraries.Click("cssselector","div.layout:nth-child(7) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
			Thread.sleep(3000);
			//System.out.println(i);
			fn_CUSIP_Fillvalues("1");
			
			driver.findElement(By.linkText("Next Fund")).click();
		}
		else
		{
			for(int i = 1;i<=no_of_Pages;i++)
			{
				ReusableLibraries.Click("cssselector","div.layout:nth-child(4) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
				ReusableLibraries.Click("cssselector","div.layout:nth-child(5) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
				ReusableLibraries.Click("cssselector","div.layout:nth-child(6) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
				ReusableLibraries.Click("cssselector","div.layout:nth-child(7) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)");
				Thread.sleep(3000);
				System.out.println(i);
				fn_CUSIP_Fillvalues(""+i);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true)", driver.findElement(By.linkText("Next Fund")));
				if(i<no_of_Pages)
				{
					driver.findElement(By.linkText(""+(i+1))).click();
				}
			}
		}
		
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
	}
	
	public void Screen5() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
	}
	
	
	public String getCaseNumber() throws InterruptedException 
	{
		try {
		WebElement CaseNumber = ReusableLibraries.selectObjects(driver, "span", "outerHTML", "20141009112850013217103");
		//CaseNumber = driver.findElement(By.cssSelector("#RULE_KEY > div > div > div > div.content-item.content-field.item-1.remove-top-spacing.remove-bottom-spacing.remove-left-spacing.flex.flex-row.dataValueRead > span"));
		//String fullcaseno = CaseNumber.getText();
		Thread.sleep(5000);
		String fullcaseno = CaseNumber.getAttribute("innerHTML");
		fullcaseno = fullcaseno.replace("(","");
		fullcaseno = fullcaseno.replace(")","");
		System.out.println("Case number is " + fullcaseno);
		return fullcaseno;
		}
		catch(StaleElementReferenceException ste)
		{
			Scanner reader = new Scanner(System.in);  // Reading from System.in
			System.out.println("Enter a number: ");
			String fullcaseno = reader.next(); // Scans the next token of the input as an int.
			//once finished
			//reader.close();
			return fullcaseno;
		}
		catch(NullPointerException nse)
		{
			Scanner reader = new Scanner(System.in);  // Reading from System.in
			System.out.println("Enter a number: ");
			String fullcaseno = reader.next(); // Scans the next token of the input as an int.
			//once finished
			//reader.close();
			return fullcaseno;
		}
	}
	
	
	

	
	public void fn_CUSIP_Fillvalues(String row_no) throws Exception 
	{
		String strMorningStarName = "MorningStarName";
		String strCategory = "Category";
		String strRFIndicator = "RFIndicator";
		String strRFPercentage = "RFPercentage";
		String strFundInceptionDate3 = "FundInceptionDate" + row_no;
		String strNetExpRatio = "NetExpRatio";
		String strGrsExpRatio = "GrsExpRatio";
		String strTicker = "Ticker";
		String strProspectusName = "ProspectusName";
		String strProspectusObjective = "ProspectusObjective";
		String strSECID = "SECID";
		String strMorningstarReturn = "MorningstarReturn";
		String strMorningStarRisk = "MorningStarRisk";
		String strShareClass = "ShareClass";
		String strProspectusExpWaiverType = "ProspectusExpWaiverType";
		String strGrsExpRatioWaiverExpDate3 = "GrsExpRatioWaiverExpDate" + row_no;
		String strClosedToAllInvestor = "ClosedToAllInvestor";
		String strClosedToAllInvestorDate3 = "ClosedToAllInvestorDate" + row_no;
		String strClosedToNewInvestor = "ClosedToNewInvestor";
		String strClosedToNewInvestorDate3 = "ClosedToNewInvestorDate"  + row_no;
		
		String strCorporateEIN = "CorporateEIN";
		String strFundFamily = "FundFamily";
		String strPrintName = "PrintName";
		String strInvestmentName = "InvestmentName";
		String strWebName = "WebName";
		String strMiddleName = "MiddleName";
		String strAgeTargetSeries = "AgeTargetSeries";
		String strAgeTargetSeriesName = "AgeTargetSeriesName";
		String strAssetClassIC300 = "AssetClassIC300";
		String strAssetClassC301 = "AssetClassC301";
		String strAssetCatIC302 = "AssetCatIC302";
		
		String strRegInceptionDate3 = "RegInceptionDate" + row_no;
		String strArchWrapFees = "ArchWrapFees";
		String strRFHoldPeriod = "RFHoldPeriod";
		String strRevenueSharing = "RevenueSharing";
		String strQDIA = "QDIA";
		String TransferRest = "TransferRestriction";
		String TransferRestAmnt = "TransferRestrictionAmt";
		String CompetingInv = "CompetingInvestment";
		
		
		String strZeroRevShareNonRegistered = "ZeroRevShareNonRegistered";
		String strZeroRevShareRegistered = "ZeroRevShareRegistered";
		String strInstitutionalNonRegister = "InstitutionalNonRegister";
		String strInstitutionalRegister = "InstitutionalRegister";
		String strRetailNonRegister = "RetailNonRegister";
		String strRetailRegister = "RetailRegister";
		
		String strIOSEn = "IOSEn";
		String strIOSSp = "IOSSp";
		String strIOSArchEn = "IOSArchEn";
		String strIOSArchSp = "IOSArchSp";
		String strPreSale408YN = "PreSale408YN";
		
		String strPriceWizardInvstName = "PriceWizardInvstName";
		String strInternalID = "InternalID";
		String strUITFundDesc = "UITFundDesc";
		String strAcctTypeCodeIC304 = "AcctTypeCodeIC304";
		String strAulFstOffinNonRegMkt3 = "AulFstOffinNonRegMkt"+ row_no;
		String strAulGLUniteCodes = "AulGLUniteCodes";
		String strPriceThruDTCC = "PriceThruDTCC";
		String strDailyAccural = "DailyAccural";
		String ReleaseDate = "ReleaseDateOMNI"+ row_no;
		String AULFirstoffer = "AulFstOffinNonRegMkt" + row_no ;
		
		
		
		ReusableLibraries.Set("id", strCorporateEIN, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "CorporateEIN"));
		ReusableLibraries.Set("id", strFundFamily, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "FundFamily"));
		ReusableLibraries.Set("id", strPrintName, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "PrintName"));
		ReusableLibraries.Set("id", strInvestmentName, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "InvestmentName"));
		String tmp_val = testData.getstringcellvalue1("AddFunds", "TC01", row_no, "WebName");
		ReusableLibraries.Set("id", strWebName,tmp_val);
		if(ReusableLibraries.Get("id", strWebName) == "" || ReusableLibraries.Get("id", strWebName).isEmpty())
		{
			ReusableLibraries.robot_Text(tmp_val);
		}
		ReusableLibraries.Set("id", strMiddleName, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "MiddleName"));
		tmp_val = testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AgeTargetSeries");
		ReusableLibraries.Set("id", strAgeTargetSeries,tmp_val );
		if(ReusableLibraries.Get("id", strAgeTargetSeries) == "" || ReusableLibraries.Get("id", strAgeTargetSeries).isEmpty())
		{
			ReusableLibraries.robot_Text(tmp_val);
		}
		
		ReusableLibraries.Set("id", strAgeTargetSeriesName, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AgeTargetSeriesName"));
		ReusableLibraries.Set("id", strAssetClassIC300, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AssetClassIC300"));
		ReusableLibraries.Set("id", strAssetClassC301, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AssetClassC301"));
		ReusableLibraries.Set("id", strAssetCatIC302, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AssetCatIC302"));
		

		ReusableLibraries.Set("id", strRegInceptionDate3, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RegInceptionDate3"));
		ReusableLibraries.Set("id", strArchWrapFees, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "ArchWrapFees"));
		
		ReusableLibraries.Set("id", strRFHoldPeriod, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RFHoldPeriod"));
		tmp_val = testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RFHoldPeriod");
		if(ReusableLibraries.Get("id", strRFHoldPeriod) == "" || ReusableLibraries.Get("id", strRFHoldPeriod).isEmpty())
		{
			ReusableLibraries.robot_Text(tmp_val);
		}
		ReusableLibraries.Set("id", strRevenueSharing, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RevenueSharing"));
		ReusableLibraries.Select("id", strQDIA, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "QDIA"));
		ReusableLibraries.Select("id", TransferRest, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "TransferRestriction")); //- Need Datatable
		//ReusableLibraries.Set("id", TransferRestAmnt, "");//- Need Datatable
		//ReusableLibraries.Set("id", TransferRestAmnt, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "TransferRestrictionAmt"));//- Need Datatable
		ReusableLibraries.Select("id", CompetingInv, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "CompetingInvestment"));//- Need Datatable
		
		ReusableLibraries.Select("id", strZeroRevShareNonRegistered, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "ZeroRevShareNonRegistered"));
		ReusableLibraries.Select("id", strZeroRevShareRegistered, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "ZeroRevShareRegistered"));
		ReusableLibraries.Select("id", strInstitutionalNonRegister, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "InstitutionalNonRegister"));
		ReusableLibraries.Select("id", strInstitutionalRegister, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "InstitutionalRegister"));
		ReusableLibraries.Select("id", strRetailNonRegister, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RetailNonRegister"));
		ReusableLibraries.Select("id", strRetailRegister, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "RetailRegister"));
	
		ReusableLibraries.Set("id", strIOSEn, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "IOSEn"));
		ReusableLibraries.Set("id", strIOSSp, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "IOSSp"));
		ReusableLibraries.Set("id", strIOSArchEn, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "IOSArchEn"));
		ReusableLibraries.Set("id", strIOSArchSp, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "IOSArchSp"));
		ReusableLibraries.Select("id", strPreSale408YN, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "PreSale408YN"));
		
		ReusableLibraries.Set("id", ReleaseDate, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "relasedateOmni"));
		ReusableLibraries.Set("id", AULFirstoffer, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "relasedateOmni"));
		
		//ReusableLibraries.Set(driver, AulFstOffinNonRegMkt3, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AulFstOffinNonRegMkt3"));
		
		/*
		ReusableLibraries.Set(driver, PriceWizardInvstName, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "PriceWizardInvstName"));
		ReusableLibraries.Set(driver, InternalID, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "InternalID"));
		ReusableLibraries.Set(driver, UITFundDesc, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "UITFundDesc"));
		ReusableLibraries.Set(driver, AcctTypeCodeIC304, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AcctTypeCodeIC304"));
		ReusableLibraries.Set(driver, AulFstOffinNonRegMkt3, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AulFstOffinNonRegMkt3"));
		ReusableLibraries.Set(driver, AulGLUniteCodes, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "AulGLUniteCodes"));
		ReusableLibraries.Set(driver, PriceThruDTCC, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "PriceThruDTCC"));
		ReusableLibraries.Set(driver, DailyAccural, testData.getstringcellvalue1("AddFunds", "TC01", row_no, "DailyAccural"));
		*/
		
	}
	
	public void fetch_MstarData(String Row_no) throws Exception
	{
		ReusableLibraries.Click("xpath","//*[@id=\"RULE_KEY\"]/div[1]/i");
		ReusableLibraries.Click("cssselector", ".Collapsed > div:nth-child(1) > div:nth-child(1) > i:nth-child(1)");
		String strMorningStarName = "MorningStarName";
		String strCategory = "Category";
		String strRFIndicator = "RFIndicator";
		String strRFPercentage = "RFPercentage";
		String strFundInceptionDate3 = "FundInceptionDate";
		String strNetExpRatio = "NetExpRatio";
		String strGrsExpRatio = "GrsExpRatio";
		String strTicker = "Ticker";
		String strProspectusName = "ProspectusName";
		String strProspectusObjective = "ProspectusObjective";
		String strSECID = "SECID";
		String strMorningstarReturn = "MorningstarReturn";
		String strMorningStarRisk = "MorningStarRisk";
		String strShareClass = "ShareClass";
		String strProspectusExpWaiverType = "ProspectusExpWaiverType";
		String strGrsExpRatioWaiverExpDate3 = "GrsExpRatioWaiverExpDate";
		String strClosedToAllInvestor = "ClosedToAllInvestor";
		String strClosedToAllInvestorDate3 = "ClosedToAllInvestorDate";
		String strClosedToNewInvestor = "ClosedToNewInvestor";
		String strClosedToNewInvestorDate3 = "ClosedToNewInvestorDate";
		
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "MorningStarName",ReusableLibraries.Get("id", strMorningStarName));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "Category",ReusableLibraries.Get("id", strCategory));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "RFIndicator",ReusableLibraries.Get("id", strRFIndicator));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "RFPercentage",ReusableLibraries.Get("id", strRFPercentage));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "FundInceptionDate3",ReusableLibraries.Get("id", strFundInceptionDate3));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "NetExpRatio",ReusableLibraries.Get("id", strNetExpRatio));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "GrsExpRatio",ReusableLibraries.Get("id", strGrsExpRatio));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "Ticker",ReusableLibraries.Get("id", strTicker));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ProspectusName",ReusableLibraries.Get("id", strProspectusName));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ProspectusObjective",ReusableLibraries.Get("id", strProspectusObjective));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "SECID",ReusableLibraries.Get("id", strSECID));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "MorningstarReturn",ReusableLibraries.Get("id", strMorningstarReturn));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "MorningStarRisk",ReusableLibraries.Get("id", strMorningStarRisk));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ShareClass",ReusableLibraries.Get("id", strShareClass));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ProspectusExpWaiverType",ReusableLibraries.Get("id", strProspectusExpWaiverType));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "GrsExpRatioWaiverExpDate3",ReusableLibraries.Get("id", strGrsExpRatioWaiverExpDate3));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ClosedToAllInvestor",ReusableLibraries.Get("id", strClosedToAllInvestor));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ClosedToAllInvestorDate3",ReusableLibraries.Get("id", strClosedToAllInvestorDate3));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ClosedToNewInvestor",ReusableLibraries.Get("id", strClosedToNewInvestor));
		testData.setstringcellvalue1("AddFunds", "TC01", Row_no, "ClosedToNewInvestorDate3",ReusableLibraries.Get("id", strClosedToNewInvestorDate3));
		
		
	}
	
}
