package Page;

import static org.testng.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Test.BaseTest;
import testDataFactory.testData;
import utility.Enums;
import utility.Enums.*;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class PageLogin extends BaseTest {
	public WebElement username,password,LoginButton,Avatar, Dashboard,Switchapps,InvestmentId;
	
	public PageLogin()
	{
		PageFactory.initElements(driver,this);
	}
				
	public void LoginasQA() throws Exception	
	{
		driver.navigate().to(testData.getstringcellvalue("Login",currTest,"Environment","QA","URL"));
		String strUserName = testData.getstringcellvalue("Login",currTest,"Environment","QA","Username");
		ReusableLibraries.Set("id", "txtUserID", strUserName);
		String strPassword = testData.getstringcellvalue("Login",currTest,"Environment","QA","Password");
		ReusableLibraries.Set("id", "txtPassword", strPassword);
		ReusableLibraries.sync(Low_Wait);
		ReusableLibraries.Set("id", "txtPassword",""+Keys.ENTER);
		ReusableLibraries.sync(High_Wait);
		//ReusableLibraries.Click("xpath", "//button[@id='sub']//*[@class='loginButtonText']");	
		
	}
	
	public void LoginasSponsorQA() throws Exception	
	{
		driver.navigate().to(testData.getstringcellvalue("Login",currTest,"Environment","SponsorQA","URL"));
		String strUserName = testData.getstringcellvalue("Login",currTest,"Environment","SponsorQA","Username");
		ReusableLibraries.Set("id", "txtUserID", strUserName);
		String strPassword = testData.getstringcellvalue("Login",currTest,"Environment","SponsorQA","Password");
		ReusableLibraries.Set("id", "txtPassword", strPassword);
		ReusableLibraries.sync(Low_Wait);
		ReusableLibraries.Set("id", "txtPassword",""+Keys.ENTER);
		ReusableLibraries.sync(High_Wait);
		//ReusableLibraries.Click("xpath", "//button[@id='sub']//*[@class='loginButtonText']");	
		
	}	
/*	
	public static void CreateCase(SpousalConsent SpousalConsent,RMD RMD,WithDrawalType WithDrawalType,WithDrawalOption WithDrawalOption,TAXWH TAXWH,Reservist Reservist) throws Exception
	{
		logs.log("########################Start of Create Case##################################");
		String qry = "";
		if(SpousalConsent == Enums.SpousalConsent.Yes)
			{
				qry = "select A1.PRTCPNT_SSN, A2.PLAN_NBR from ods.PRTCPNT_REF A1 inner join ods.plan_ref A2 on(A1.plan_REF_GEN_ID = A2.plan_REF_GEN_ID) inner join ods.PLAN_SVC_VW A3 on(A2.Plan_NBR = A3.planNumber and DistributionSpousalConsentRequiredIndicator like 'Y' and plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY')";
			}
		else
			{
				qry = "select A1.PRTCPNT_SSN, A2.PLAN_NBR from ods.PRTCPNT_REF A1 inner join ods.plan_ref A2 on(A1.plan_REF_GEN_ID = A2.plan_REF_GEN_ID) inner join ods.PLAN_SVC_VW A3 on(A2.Plan_NBR = A3.planNumber and DistributionSpousalConsentRequiredIndicator like 'N' and plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY')";
			}
//--------------------------------------------------------		
		if(RMD == Enums.RMD.Yes)
			{
				qry = qry + " intersect Select ParticipantNumber,PlanNumber from ods.PRTCPNT_PLAN_ATTR_VW where PlanNumber in(Select PlanNumber from ods.plan_ref where plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY') and MinimumDistributionSuppressIndicator like '0'";
			}
		else
			{
				qry = qry + " intersect Select ParticipantNumber,PlanNumber from ods.PRTCPNT_PLAN_ATTR_VW where PlanNumber in(Select PlanNumber from ods.plan_ref where plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY') and MinimumDistributionSuppressIndicator like '1'";
			}
//----------------------------------------------------------		
		if(WithDrawalType == Enums.WithDrawalType.Full)
			{
				
			}
		else
			{
			
			}
//---------------------------------------------------------
		if(TAXWH == Enums.TAXWH.NA)
			{
				
			}
		else if(TAXWH == Enums.TAXWH.NO)
			{
				
			}
		else
			{
				
			}
//------------------------------------------------------
		qry = qry + " order by PRTCPNT_SSN";
		
		String Participant_Nbr = dbRetrieve(qry,"PRTCPNT_SSN");
		String Plan_Nbr = dbRetrieve(qry,"PLAN_NBR");
		
		//String Participant_Nbr = "553761761";
		//String Plan_Nbr = "G38317";
		
		
		Login.LoginasQA();
		selectPlan.startMyDistribution();
		selectPlan.SelectPlanid(Participant_Nbr,Plan_Nbr);
		selectPlan.selectParLink();
		
		DistributionOptions.selectSFS();
		
		ConfirmStatus.QDRO_No();
		if(SpousalConsent == Enums.SpousalConsent.Yes)
			{
				
				ConfirmStatus.RMD_Next();
				ConfirmStatus.Spousal_Download();
				ConfirmStatus.Spousal_Upload();	
			}

			AccountBalance.clickNext();
//------------------------------------------------------------------------
			if(WithDrawalType == Enums.WithDrawalType.Full)
			{
				WithdrawalOptions.clickFull();
			}
		else
			{
			WithdrawalOptions.clickPartial();
			}
//------------------------------------------------------------------------------------
			if(WithDrawalOption == Enums.WithDrawalOption.InternalRollOver)
			{
				WithdrawalOptions.selectRollOver();
				WithdrawalOptions.selectInternalRollOver();
			}
			else if(WithDrawalOption == Enums.WithDrawalOption.ExternalRollover)
			{
				WithdrawalOptions.selectRollOver();
				WithdrawalOptions.selectExternalRollOver();
			}
			else if(WithDrawalOption == Enums.WithDrawalOption.Check)
			{
				WithdrawalOptions.selectCash();
				WithdrawalOptions.selectCheck();
			}
			else
			{
				WithdrawalOptions.selectCash();
				WithdrawalOptions.selectACH();
			}
//------------------------------------------------------------------------			
			if(TAXWH == Enums.TAXWH.NA)
			{
				TaxElections.TaxElection_NA();
			}
		else if(TAXWH == Enums.TAXWH.NO)
			{
				TaxElections.TaxElection_No();
			}
		else
			{
				TaxElections.TaxElection_Yes();
			}
//-----------------------------------------------------------------------------
			if(Reservist == Enums.Reservist.Yes)
			{
				Reservists.Reservist_Yes();
			}
			else
			{
				Reservists.Reservist_No();
			}
			
			Summary.Summary_Next();
			Summary.TermsConditions();
			System.out.println(Summary.getCaseNo());
	}
*/
	
	public void LoginasDEV() throws Exception	
	{
		driver.navigate().to(testData.getstringcellvalue("Login",currTest,"Environment","DEV","URL"));
		String strUserName = testData.getstringcellvalue("Login",currTest,"Environment","DEV","Username");
		ReusableLibraries.Set("id", "txtUserID", strUserName);
		String strPassword = testData.getstringcellvalue("Login",currTest,"Environment","DEV","Password");
		ReusableLibraries.Set("id", "txtPassword", strPassword);
		ReusableLibraries.Click("id", "sub");	
		
	}
	
	public void LoginasA3() throws Exception	
	{
		String strUserName = testData.getstringcellvalue("Login",currTest,"Role","A3","Username");
		ReusableLibraries.Set("id", "txtUserID", strUserName);
		String strPassword = testData.getstringcellvalue("Login",currTest,"Role","A3","Password");
		ReusableLibraries.Set("id", "txtPassword", strPassword);
		ReusableLibraries.Click("id", "sub");	
		
	}
	
	public void LoginasA4() throws Exception	
	{
		String strUserName = testData.getstringcellvalue("Login",currTest,"Role","A4","Username");
		ReusableLibraries.Set("id", "txtUserID", strUserName);
		String strPassword = testData.getstringcellvalue("Login",currTest,"Role","A4","Password");
		ReusableLibraries.Set("id", "txtPassword", strPassword);
		ReusableLibraries.Click("id", "sub");	
		
	}
	
	public void confirm_Login() 
	{
		WebElement Avatar1 = null;
		List<WebElement> Avatars = driver.findElements(By.tagName("i"));
		for(WebElement Avatar: Avatars)
		{
			if(Avatar.getAttribute("outerHTML").contains("icons avatar name-s "))
			{
				Avatar1 = Avatar;
			}
		}
		
		if(Avatar1.isDisplayed())
		{
			htmlReports.reportPASS("Logged in Successfully as expected");
			
		}
		else
		{
			htmlReports.reportFAIL("Login Failed");
		}
		
	}
	
	public void Logout() throws InterruptedException
	
	{
		driver.switchTo().defaultContent();
		ReusableLibraries.Click("xpath","//i[@data-test-id='px-opr-image-ctrl']");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("linktext", "Log off");
	}
	
	public void switch_application() throws InterruptedException 
	{
		Thread.sleep(3000);
		
			//WebElement txtCases = driver.findElement(By.id("EXPAND-INNERDIV"));
			//new WebDriverWait(driver,10).until(ExpectedConditions.attributeContains(txtCases, "innerHTML", "Cases"));
		ReusableLibraries.switchDefaultFrame();	
		//ReusableLibraries.waitFor(Avatar);
		ReusableLibraries.Click("xpath","//i[@data-test-id='px-opr-image-ctrl']");
		Actions act = new Actions(driver);
		
		Switchapps = driver.findElement(By.xpath("//*[@id=\"menu-item-$PpyNavigation1548793229824$ppyElements$l1$ppyElements$l1\"]/a/span/span"));
		//act.sendKeys(Keys.ARROW_RIGHT).build().perform();
		act.moveToElement(Switchapps).build().perform();
		
		InvestmentId = driver.findElements(By.linkText("Disbursement")).get(1);
		ReusableLibraries.Click(InvestmentId);

		WebElement Dashboard = driver.findElement(By.xpath("//span[@class='menu-item-title'][contains(text(),'Dashboard')]"));
		ReusableLibraries.waitFor(Dashboard);
		if(Dashboard.isDisplayed())
		{
			htmlReports.reportPASS("Dashboard displayed Successfully as expected");
		}
		else
		{
			htmlReports.reportFAIL("Dashboard not displayed Successfully as expected");
		}
	}
	
	
	
	
	


}
