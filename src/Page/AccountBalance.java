package Page;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class AccountBalance extends BaseTest {

	public Double getAccountBalance() throws InterruptedException
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		//WebElement accntBalance = ReusableLibraries.getElement("cssSelector", ".centerJustifyStyle");
		WebElement accntBalance = ReusableLibraries.getElement("name", "$PpyWorkPage$pParticipant$pPlan$pTotalVestedAmt");
		//ReusableLibraries.selectObjectsbyTag("span", "outerHTML", "data-test-id=\"201806061206040310301704\" data-ctl=\"Text\" class=\"centerJustifyStyle\"");
		System.out.println("Account Balance is " + accntBalance.getAttribute("innerText"));
		String str1 = accntBalance.getAttribute("innerText");
		str1 = str1.replaceAll("\\$", "");
		str1 = str1.replaceAll(",", "");
		Double amt = Double.parseDouble(str1);
		return amt;
	}
	
	public Double getTotalBalancebySource() throws Exception
	{
		double total = 0;
		int noofRows = driver.findElements(By.xpath("/html/body/div[2]/form/div[3]/div/section/div/div/div/div/div/div/div/div/div/div/div[3]/div/div[4]/div/div/div/div/div[1]/table/tbody/tr/td[2]/div/table/tbody/tr")).size();
		for(int i = 2; i <= noofRows;i++)
		{
			WebElement rowval = ReusableLibraries.getElement("xpath", "/html/body/div[2]/form/div[3]/div/section/div/div/div/div/div/div/div/div/div/div/div[3]/div/div[4]/div/div/div/div/div[1]/table/tbody/tr/td[2]/div/table/tbody/tr[" + i + "]/td[2]/div/span/span");
			try 
			{
				if(rowval.getAttribute("innerText").contains("$"))
				{
					String amt = rowval.getAttribute("innerText").replaceAll("\\$", "").replaceAll(",","");
					Double amt1 = Double.parseDouble(amt);
					total = total + amt1;
				}
			}
			catch(NullPointerException npe)
			{
				total = total + 0;
			}
		}
		htmlReports.addScreenshot(currFolder +"\\BalanceDetails" + currIteration + ".jpg" );
		System.out.println("total is " + total);
		return total;
	}
	
	public void validate_Balances(String planNum, String Participant) throws Exception
	{
		 // Initializing a Dictionary 
        Dictionary source = new Hashtable(); 
        //Dictionary serviVal = new Hastable();
        
		int noofRows = driver.findElements(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr")).size();
		for(int i = 2; i<=noofRows;i++)
		{
			WebElement PlanSource = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td/div/span"));
			WebElement SourceBalance = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[2]/div/span/span"));
			source.put(PlanSource.getAttribute("innerText"), SourceBalance.getAttribute("innerText").replace("$","").replace(",","")); 
		}
		
		String uri1 = testData.getstringcellvalue("Login",currTest,"Environment","QA","URI1");
		String uri2 = testData.getstringcellvalue("Login",currTest,"Environment","QA","URI2");
		
		String URI1 =   uri1+planNum+uri2;//"https://stgesb.oneamerica.com:61018/pegaapi/plans/" + planNum + "/participants/participant";
		 
		 HttpClient client = new DefaultHttpClient();
		  //String URI = URI1 + planID + URI2 + confirmno;
		  HttpGet request = new HttpGet(URI1);
		  request.addHeader("client_id", "fa74ee11ef9b42d184abecf54b2e5577");
		  request.addHeader("client_secret", "eBE0a092434f471691513302567FFE94");
		  request.addHeader("X-OA-ParticipantId", Participant);
		  request.addHeader("X-OA-ClientId","PEGA");
		  request.addHeader("Content-Type", "application/json");
		  
		  HttpResponse response = client.execute(request);
		  if(response.getStatusLine().getStatusCode() == 200)
		  {
			  //System.out.println(response.getEntity()
			  String jsonString = EntityUtils.toString(response.getEntity());
			  JSONObject arr = new JSONObject(jsonString.trim());
			  JSONArray jarray = arr.getJSONArray("balanceByPlanType");
			  jarray = jarray.getJSONObject(0).getJSONArray("contributionSources");

			 // serviVal
			  for(int x = 0;x<jarray.length();x++)
			  {
				  String BalAmt = jarray.getJSONObject(x).get("vestedBalanceAmount").toString();
				  //BalAmt.replace("$", "");
				  String expBalAmt = source.get(jarray.getJSONObject(x).get("contributionSourceFullName")).toString();
				  
				  if(expBalAmt.equalsIgnoreCase(BalAmt))
				  {
					 htmlReports.reportPASS("Value matches as expected - " + jarray.getJSONObject(x).get("contributionSourceFullName").toString() + "  is " + expBalAmt);
				  }
				  else
				  {
					  htmlReports.reportFAIL("Value doesn't match as expected - " + jarray.getJSONObject(x).get("contributionSourceFullName").toString() + "  Exp is " + expBalAmt + " but the actual is " + BalAmt);
				  }
			  }
		  }
		  
	}
	
	public void clickNext() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\AccountBalance" + currIteration + ".jpg" );
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		//ReusableLibraries.Click("cssSelector", ".workarea-view-footer > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1) > button:nth-child(1)");
	}
	
	
	public void selectFullWithdrawalType() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\WithdrawalType_Full" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector", "span.col-3:nth-child(1) > label:nth-child(2)");
	}
	
	public void selectRollOverAccount() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\RollOverAccount" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector",".layout-content-inline_grid_double > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > label:nth-child(2)");
	}
	
	public void selectPayee_ExternalRollOver() throws Exception 
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Select("name","$PpyWorkPage$pDisbursement$pPayeeList$l1$pPayeeType", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "PayeeType"));
		ReusableLibraries.Set("id", "AllocationPercentage", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "AllocationPercentage"));
		ReusableLibraries.Set("id", "pyName", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "PayeeName"));
		ReusableLibraries.Select("id","AccountType", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "AccountType"));
		ReusableLibraries.Set("id", "AccountNumber", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "AccountNumber"));
		ReusableLibraries.Set("id", "pyStreet", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Street"));
		ReusableLibraries.Set("id", "pyCity", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "City"));
		ReusableLibraries.Select("id","pyState", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "State"));
		ReusableLibraries.Set("id", "pyPostalCode", testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "PostalCode"));
		htmlReports.addScreenshot(currFolder +"\\PayeeDetails" + currIteration + ".jpg" );
		clickNext();
	}
	
	public String getCaseNumber() throws InterruptedException 
	{
		try {
			Thread.sleep(5000);
			ReusableLibraries.switchFrame();
			WebElement CaseNumber = ReusableLibraries.getElement("xpath", "/html/body/div[3]/form/div[3]/header/div/div[1]/div/div/div[1]/div/div/div/div/div/div/div/div[2]/div/div/div/div/div[1]/span");
			ReusableLibraries.sync(High_Wait);
			//CaseNumber = driver.findElement(By.cssSelector("#RULE_KEY > div > div > div > div.content-item.content-field.item-1.remove-top-spacing.remove-bottom-spacing.remove-left-spacing.flex.flex-row.dataValueRead > span"));
			//String fullcaseno = CaseNumber.getText();
			Thread.sleep(5000);
			String fullcaseno = CaseNumber.getAttribute("innerHTML");
			fullcaseno = fullcaseno.replace("(","");
			fullcaseno = fullcaseno.replace(")","");
			System.out.println("Case number is " + fullcaseno);
			return fullcaseno;
		}
		catch(StaleElementReferenceException ste)
		{
			Scanner reader = new Scanner(System.in);  // Reading from System.in
			System.out.println("Enter a number: ");
			String fullcaseno = reader.next(); // Scans the next token of the input as an int.
			//once finished
			//reader.close();
			return fullcaseno;
		}
		catch(NullPointerException nse)
		{
			Scanner reader = new Scanner(System.in);  // Reading from System.in
			System.out.println("Enter a number: ");
			String fullcaseno = reader.next(); // Scans the next token of the input as an int.
			//once finished
			//reader.close();
			return fullcaseno;
		}
	}
	
	public String[][] getTransactionNos() throws InterruptedException
	{
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		
		ReusableLibraries.sync(High_Wait);
		String[][] retString = new String[5][5];
		try {
			int Rowcnt = driver.findElements(By.xpath("//*[@pl_prop_class=\"OA-FW-FinSvc-Data-Transaction\"]/tbody/tr")).size();
			//int Rowcnt = 2;
			//if(Rowcnt >=2)
			ReusableLibraries.sync(High_Wait);
			if(driver.findElement(By.xpath("//*[@pl_prop_class=\"OA-FW-FinSvc-Data-Transaction\"]/tbody/tr[2]")).getAttribute("id").contains("PpyWorkPage$pTransactionList"))
			{
				retString = new String[Rowcnt-1][5];
				for(int i = 2; i<=Rowcnt;i++)
				{
					String fileId = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[1]/div/span")).getText();
					String tranStatus = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[2]/div/span")).getText();
					String userId = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[3]/div/span")).getText();
					String tradeDate = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[4]/div/span")).getText();
					String seqNo = driver.findElement(By.xpath("//*[@id=\"bodyTbl_right\"]/tbody/tr[" + i + "]/td[5]/div/span")).getText();
									
					retString[i-2][0] = fileId;
					retString[i-2][1] = tranStatus;
					retString[i-2][2] = userId;
					retString[i-2][3] = tradeDate;
					retString[i-2][4] = seqNo;
				}
			}
			else
			{
				Thread.sleep(30000);
				//driver.navigate().refresh();
				//ReusableLibraries.switchFrame();
				
				ReusableLibraries.Click("cssSelector","#RULE_KEY > div.layout.layout-noheader.layout-noheader-workarea_header > div > div > div.content-item.content-layout.item-2.remove-all-spacing.float-right.set-width-auto.flex.flex-row.align-end > div > div > div > div.content-item.content-field.item-2.remove-top-spacing.remove-bottom-spacing.flex.flex-row.dataValueRead > span > button");
				ReusableLibraries.Click("cssSelector","#pyNavigation1525275614988 > li:nth-child(1) > a > span > span");
				ReusableLibraries.sync(High_Wait);
				retString = getTransactionNos();
			}
			
		}
		catch(NullPointerException npe)
		{
			retString[0][0] = "null";
		}
		
		return retString;
	}

	
	public void specialTaxNotice() throws Exception
	{
		System.out.println(driver.getWindowHandles().size());
		ReusableLibraries.Click("partiallinktext", "Special Tax Notice");
		ReusableLibraries.sync(High_Wait);
		System.out.println(driver.getWindowHandles().size());
		
		/*ReusableLibraries.switchDefaultFrame();
		
		try 
		{
			driver.switchTo().alert().dismiss();
			htmlReports.reportPASS("Pop up displayed as expected");
		}
		catch(NoAlertPresentException ae)
		{
			htmlReports.reportFAIL("Pop up not displayed as expected");
		}
		
		*/
		
	}

}

