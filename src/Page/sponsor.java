package Page;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class sponsor extends BaseTest {

	
	public void BeginCase(String CaseNo) throws Exception
	{
		htmlReports.reportINFO("------- Searching for Case and Begin----");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchDefaultFrame();
	
		driver.findElement(By.id("pySearchText")).clear();
		ReusableLibraries.Set("id", "pySearchText", CaseNo + Keys.ENTER);
		Thread.sleep(3000);
		ReusableLibraries.switchFrame();
		htmlReports.addScreenshot(currFolder +"\\Sponsor_SearchCase" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Secondary pzhc pzbutton']");
	}
	
	public String approveCase() throws Exception
	{
		htmlReports.reportINFO("------- Check for TermDate, if Blank enter Current Date and click on Approve---");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		
		ReusableLibraries.sync(High_Wait);
		String termDate = driver.findElement(By.id("TermDate")).getAttribute("value");  //ReusableLibraries.GetValue("id", "TermDate", "value");
		ReusableLibraries.sync(Low_Wait);
		htmlReports.addScreenshot(currFolder +"\\Sponsor_TradeDate" + currIteration + ".jpg" );
		
//----------------------Including Summary validation with API---------Feb 5th 2019----------------
		String Participant_Nbr = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+currIteration, "Participant");
		String Plan_Nbr =  testData.getstringcellvalue1("CreateCase", "CreateCase", ""+currIteration, "Plan");
		Sponsor_verifyDataSummary(Plan_Nbr,Participant_Nbr);
//----------------------Including Summary validation with API---------Feb 5th 2019----------------
		
		if(termDate.isEmpty())
		{
			ReusableLibraries.Click("xpath", "/html/body/div[2]/form/div[3]/div/section/div/div/div/div/div/div/div/div/div/div/div[3]/div/div[2]/div[2]/div/div[2]/div/div/div/div/div[1]/div/div/div/div[1]/div/span/i/img");
			ReusableLibraries.sync(Low_Wait);
			driver.findElement(By.id("TermDate")).sendKeys(new SimpleDateFormat("MM/dd/yyyy").format(Calendar.getInstance().getTime()) + Keys.TAB);
			htmlReports.addScreenshot(currFolder +"\\Sponsor_Updated_TradeDate" + currIteration + ".jpg" );
		}
		
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		ReusableLibraries.sync(High_Wait);

		
		driver.findElement(By.id("SponsorSignature")).sendKeys("Approver" + Keys.TAB);
		ReusableLibraries.sync(Low_Wait);
		ReusableLibraries.Click("xpath", "/html/body/div[2]/form/div[3]/div/section/div/div/div/div/div/div/div/div/div/div/div[3]/div/div/div/div/div[3]/div/div/div[1]/span/label");
		htmlReports.addScreenshot(currFolder +"\\Sponsor_Approve" + currIteration + ".jpg" );
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("xpath", "//button[@class='Main_Content_Primary_Button pzhc pzbutton']");
		
		ReusableLibraries.sync(High_Wait);
		String Date1 = driver.findElement(By.xpath("//*[@class='content-item content-field item-3 flex explanation_body_dataLabelRead centered']")).getAttribute("innerText");
		htmlReports.addScreenshot(currFolder +"\\Sponsor_ApprovedTime" + currIteration + ".jpg" );
		
		return Date1;
	}

	public void Sponsor_verifyDataSummary(String planNum, String Participant) throws Exception
	{
		htmlReports.reportINFO("----Start of Sponsor Summary screen Validation----");
		Date empdate,TermDate,empdate_Pega,TermDate_Pega;
		String state,dateofHire,email;
		empdate = TermDate=empdate_Pega=TermDate_Pega=null;
		state = dateofHire = email ="";
		 @SuppressWarnings("deprecation")
		HttpClient client = new DefaultHttpClient();
		  //String URI = URI1 + planID + URI2 + confirmno;
		 String uri1 = testData.getstringcellvalue("Login",currTest,"Environment","QA","URI1");
		 String uri2 = "/participants/participant";
		 String URI1 = uri1 +planNum+ uri2;
		  HttpGet request = new HttpGet(URI1);
		  request.addHeader("client_id", "fa74ee11ef9b42d184abecf54b2e5577");
		  request.addHeader("client_secret", "eBE0a092434f471691513302567FFE94");
		  request.addHeader("X-OA-ParticipantId", Participant);
		  request.addHeader("X-OA-ClientId","PEGA");
		  request.addHeader("Content-Type", "application/json");
		  
		  HttpResponse response = client.execute(request);
		  if(response.getStatusLine().getStatusCode() == 200)
		  {
			  //System.out.println(response.getEntity()
			  String jsonString = EntityUtils.toString(response.getEntity());
			 // jsonString = jsonString.replace("{", "[");
			  //logs.log(jsonString);
			  htmlReports.reportINFO(jsonString);
			  
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			  SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
			  
			  //JSONArray arr = new JSONArray(jsonString.trim());
			  JSONObject arr1 = new JSONObject(jsonString.trim());
			  String participantFullName = arr1.get("participantFullName").toString();
			  String participantFormattedAge = arr1.get("participantFormattedAge").toString();	  
			  
			  String participantEmploymentDate = arr1.get("participantEmploymentDate").toString();
			  if(!(participantEmploymentDate.equals("null")))
			  {
				  empdate = sdf.parse(participantEmploymentDate);
			  }
			  
			  String participantStateCode = arr1.getJSONObject("address").get("participantStateCode").toString();
			  if(participantStateCode.equals("null"))
			  {
				  participantStateCode = "";
			  }
			  String participantTerminationDate = arr1.get("participantTerminationDate").toString();
			  if(!(participantTerminationDate.equals("null")))
			  {
				  TermDate = sdf.parse(participantTerminationDate);
			  }
			  String participantEligibilityStatusCode = arr1.get("participantEligibilityStatusCode").toString();
			  String participantEmailAddress = arr1.get("participantEmailAddress").toString();
//------------Start of Fetch Values from PEGA App--------------------------			  
			  if(driver.findElements(By.xpath("//*[@id=\"RULE_KEY\"]/div/div/div/div[4]/div/span/span")).size() > 0)
			  {
				  email = ReusableLibraries.GetValue("xpath", "//*[@id=\"RULE_KEY\"]/div/div/div/div[4]/div/span/span", "value");
			  }
			  else
			  {
				  email = "";
			  }
			  String fullName = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230795112103']", "innerHTML");
			  String age = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230797113923' and @class='leftJustifyStyle']", "innerHTML");
			  if(driver.findElements(By.xpath("//*[@data-test-id='201809201102230799115640']")).size() > 0)
			  {
				  dateofHire = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230799115640']", "innerHTML");
				  empdate_Pega = sdf1.parse(dateofHire);
				  ReusableLibraries.Date_Compare(empdate, empdate_Pega, "Hire Date");
			  }
			  else
			  {
				  dateofHire = "";
			  }
			  if(driver.findElements(By.xpath("//*[@data-test-id='201809201102230799116355']")).size() > 0)
			  {
				  state = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230799116355']", "innerHTML");
			  }
			  else
			  {
				  state = "";  
			  }
			  String dateofTermination = ReusableLibraries.GetValue("xpath", "//input[@data-test-id='20180920110929000183709']", "value");
			  if(!(dateofTermination.isEmpty()))
			  {
				  TermDate_Pega = sdf1.parse(dateofTermination);
				  ReusableLibraries.Date_Compare(TermDate, TermDate_Pega, "TermDate");
			  }
			  
			  String empStatus = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='20180920110929000284282']", "innerHTML");
//------------Start of Compare Values--------------------------			  
			  ReusableLibraries.str_compare(participantFullName, fullName, "participantFullName");
			  ReusableLibraries.str_contains(participantFormattedAge, age, "Age");
			  ReusableLibraries.str_compare(participantStateCode, state, "State");			  
			  ReusableLibraries.str_compare(participantEligibilityStatusCode, empStatus, "EligibilityStatusCode");
			  ReusableLibraries.str_compare(participantEmailAddress, email, "EmailAddress");
			  
			  htmlReports.reportINFO("----End of Sponsor Summary screen Validation----");
			  
		  }  
	}	
}
