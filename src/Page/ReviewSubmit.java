package Page;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;

public class ReviewSubmit extends BaseTest {
	
	public void clickMilitaryYes() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("cssSelector","#RULE_KEY > div.layout.layout-none > div > div > div > div > div > div > div.content-item.content-field.item-1.flex.flex-row.dataValueWrite > span > button");
	}
	
	public void clickMilitaryNo() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("cssSelector","#RULE_KEY > div.layout.layout-none > div > div > div > div > div > div > div.content-item.content-field.item-2.flex.flex-row.dataValueWrite > span > button");
	}
	
	public void Submit_TC() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		//ReusableLibraries.Click("cssSelector","#RULE_KEY > div > div > div > div.content-item.content-field.item-6.flex.flex-row.centered.dataValueWrite > span > button");
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		
		ReusableLibraries.selectObjectsbyTag("label", "outerHTML", "chkbxCaptionRight").click();
		ReusableLibraries.Set("id", "ParticipantSignature", "TestUser1");
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		
		//add signature, checkbox & Next
	}
	
	
	public void verifySuccess() throws Exception
	{
		String CaseResult = ReusableLibraries.GetValue("cssSelector", "#RULE_KEY > div:nth-child(2) > div > div > div.content-item.content-label.item-1.flex.flex-row.centered.dataLabelWrite.main_title_question_dataLabelWrite", "innerText");
		if(CaseResult.contains("Success"))
		{
			htmlReports.reportPASS("Case created Successfully");
		}
		else
		{
			htmlReports.reportFAIL("Case could not be created Successfully");
		}	
	}
	
	
	public String fetchCaseNo() throws Exception
	{
		String CaseNum = ReusableLibraries.GetValue("cssSelector", "#RULE_KEY > div:nth-child(2) > div > div > div.content-item.content-field.item-3.flex.explanation_body_dataLabelRead.centered > div > span", "innerText");
		return CaseNum;
	}
	
}
