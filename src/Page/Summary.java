package Page;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class Summary extends BaseTest {

	
	public void Summary_Next() throws Exception
	{
		
		htmlReports.reportINFO("------- In Review Screen----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\SummaryScreen" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
	}
	
	public void TermsConditions() throws Exception
	{
		htmlReports.reportINFO("------- In Terms & Conditions Screen----");
		ReusableLibraries.sync(High_Wait);
		String sign = "//input[@name='$PpyWorkPage$pParticipant$pParticipantSignature']";
		ReusableLibraries.Set("xpath", "//input[@name='$PpyWorkPage$pParticipant$pParticipantSignature']","Test12");
		ReusableLibraries.sync(Low_Wait);
		//ReusableLibraries.Click("xpath", "//input[@id='ParticipantConsent']");
		ReusableLibraries.Click("xpath","//label[@class='chkbxCaptionRight']");
		
		//ReusableLibraries.sync(High_Wait);	
		//driver.findElement(By.id("ParticipantConsent")).sendKeys(Keys.SPACE);
		ReusableLibraries.sync(Low_Wait);
		htmlReports.addScreenshot(currFolder +"\\TermsConditions" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Main_Content_Primary_Button pzhc pzbutton']");
	}
	
	public String getCaseNo() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\SuccessScreen" + currIteration + ".jpg" );
		String Case = ReusableLibraries.GetValue("xpath", "/html/body/div[2]/form/div[3]/div[2]/section/div/div/div/div/div[3]/div/div[2]/div/div/div[4]/div/span", "innerText");
		return Case;
	}
	
	public void verifyDataSummary(String planNum, String Participant) throws Exception
	{
		Date empdate,TermDate,empdate_Pega,TermDate_Pega;
		empdate = TermDate=empdate_Pega=TermDate_Pega=null;
		
		 @SuppressWarnings("deprecation")
		HttpClient client = new DefaultHttpClient();
		  //String URI = URI1 + planID + URI2 + confirmno;
		 String uri1 = testData.getstringcellvalue("Login",currTest,"Environment","QA","URI1");
		 String uri2 = "/participants/participant";
		 String URI1 = uri1 +planNum+ uri2;
		  HttpGet request = new HttpGet(URI1);
		  request.addHeader("client_id", "fa74ee11ef9b42d184abecf54b2e5577");
		  request.addHeader("client_secret", "eBE0a092434f471691513302567FFE94");
		  request.addHeader("X-OA-ParticipantId", Participant);
		  request.addHeader("X-OA-ClientId","PEGA");
		  request.addHeader("Content-Type", "application/json");
		  
		  HttpResponse response = client.execute(request);
		  if(response.getStatusLine().getStatusCode() == 200)
		  {
			  //System.out.println(response.getEntity()
			  String jsonString = EntityUtils.toString(response.getEntity());
			 // jsonString = jsonString.replace("{", "[");
			  //logs.log(jsonString);
			  htmlReports.reportINFO(jsonString);
			  
			  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			  SimpleDateFormat sdf1 = new SimpleDateFormat("MM/dd/yyyy");
			  
			  //JSONArray arr = new JSONArray(jsonString.trim());
			  JSONObject arr1 = new JSONObject(jsonString.trim());
			  String participantFullName = arr1.get("participantFullName").toString();
			  String participantFormattedAge = arr1.get("participantFormattedAge").toString();	  
			  
			  String participantEmploymentDate = arr1.get("participantEmploymentDate").toString();
			  if(!(participantEmploymentDate.equals("null")))
			  {
				  empdate = sdf.parse(participantEmploymentDate);
			  }
			  
			  String participantStateCode = arr1.getJSONObject("address").get("participantStateCode").toString();
			  if(participantStateCode.equals("null"))
			  {
				  participantStateCode = "";
			  }
			  String participantTerminationDate = arr1.get("participantTerminationDate").toString();
			  if(!(participantTerminationDate.equals("null")))
			  {
				  TermDate = sdf.parse(participantTerminationDate);
			  }
			  String participantEligibilityStatusCode = arr1.get("participantEligibilityStatusCode").toString();
			  String participantEmailAddress = arr1.get("participantEmailAddress").toString();
			  
			  String email = ReusableLibraries.GetValue("xpath", "//*[@id='pyEmail1']", "value");
			  String fullName = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230795112103']", "innerHTML");
			  String age = ReusableLibraries.GetValue("xpath", "/html/body/div[2]/form/div[3]/div/section/div/div/div/div/div/div/div/div/div/div/div[3]/div/span/div/div[3]/div[2]/div/div/div/div/div/div/div[2]/div/span/span", "innerHTML");
			  String dateofHire = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230799115640']", "innerHTML");
			  if(!(dateofHire.isEmpty()))
			  {
				  empdate_Pega = sdf1.parse(dateofHire);
				  ReusableLibraries.Date_Compare(empdate, empdate_Pega, "Employment Date");
			  }
			  String state = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='201809201102230799116355']", "innerHTML");
			  String dateofTermination = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='20180920110929000183709']", "innerHTML");
			  if(!(dateofTermination.isEmpty()))
			  {
				  TermDate_Pega = sdf1.parse(dateofTermination);
				  ReusableLibraries.Date_Compare(TermDate, TermDate_Pega, "TermDate");
			  }
			  
			  String empStatus = ReusableLibraries.GetValue("xpath", "//*[@data-test-id='20180920110929000284282']", "innerHTML");
			  
			  ReusableLibraries.str_compare(participantFullName, fullName, "participantFullName");
			  ReusableLibraries.str_contains(participantFormattedAge, age, "Age");
			  ReusableLibraries.str_compare(participantStateCode, state, "State");			  
			  ReusableLibraries.str_compare(participantEligibilityStatusCode, empStatus, "EligibilityStatusCode");
			  ReusableLibraries.str_compare(participantEmailAddress, email, "EmailAddress");
			  
		  }  
	}
}
