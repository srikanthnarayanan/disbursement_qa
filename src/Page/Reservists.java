package Page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class Reservists extends BaseTest {

	
	public void Reservist_Yes() throws Exception
	{
		htmlReports.reportINFO("------- In Military Reservists Screen----");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("xpath", "//*[@name='MilitaryConfirmation_pyWorkPage_5']");
	}
	
	public void Reservist_No() throws Exception
	{
		htmlReports.reportINFO("------- In Military Reservists Screen----");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Click("xpath", "//*[@name='MilitaryConfirmation_pyWorkPage_6']");
	}
	
}
