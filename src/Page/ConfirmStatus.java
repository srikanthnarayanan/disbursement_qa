package Page;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class ConfirmStatus extends BaseTest {
	

	public void QDRO_Yes() throws Exception
	{
		htmlReports.reportINFO("------- In QDRO Screen----");
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		QDRO_TextCheck();
		ReusableLibraries.Click("cssSelector", "#RULE_KEY > div.layout.layout-none > div > div > div > div > div > div > div.content-item.content-field.item-1.flex.flex-row.dataValueWrite > span > button");
	}
	
	public void QDRO_No() throws Exception
	{
		htmlReports.reportINFO("------- In QDRO Screen----");
		//ReusableLibraries.Click("cssSelector", "div.dataValueWrite:nth-child(2) > span:nth-child(1) > button:nth-child(1)");
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		QDRO_TextCheck();
		htmlReports.addScreenshot(currFolder +"\\QDRO" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//*[@class='content-item content-field item-2 flex flex-row dataValueWrite']//button[@class='Main_Content_Primary_Button pzhc pzbutton']");
	}
	
	
	public void QDRO_TextCheck() throws Exception
	{
		ReusableLibraries.switchFrame();
		String ActText = ReusableLibraries.GetValue("xpath", "//*[@class='content-item content-label item-1 flex flex-row dataLabelWrite main_title_question_dataLabelWrite']", "innerHTML");
		String ExpText = "Are there pending plan benefits that may be payable under a Qualified Domestic Relations Order (QDRO)?";
		if(ExpText.equalsIgnoreCase(ActText))
		{
			htmlReports.reportPASS("QDRO Text is displayed as expected. The Value is "  + ExpText);
		}
		else
		{
			htmlReports.reportPASS("QDRO Text is not displayed as expected. The Exp Value is "  + ExpText + " and the Act value is " + ActText);
		}
	}
	
	public void empRep_Message() throws InterruptedException
	{
		String val = ReusableLibraries.GetValue("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-3.flex.flex-row > div > div > div > div", "innerText");
		if(val.equals("Your distribution may not be requested at this time. Additional documentation must be reviewed. If you have questions, please contact your employer representative."))
		{
			htmlReports.reportPASS("Employee Representative message is displayed as expected. The Value is "  + val);
		}
		else
		{
			htmlReports.reportFAIL("Employee Representative message is not displayed as expected. Actual value displayed is " + val);
		}
	}
	
	public void PartSerCenter_Message() throws InterruptedException
	{
		String val = ReusableLibraries.GetValue("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-3.flex.flex-row > div > div > div > div", "innerText");
		if(val.equals("Your distribution may not be requested at this time. Additional documentation must be reviewed. If you have questions, please contact the participant service center."))
		{
			htmlReports.reportPASS("Participant Service Center message is displayed as expected. The Value is "  + val);
		}
		else
		{
			htmlReports.reportFAIL("Participant Service Center message is not displayed as expected. Actual value displayed is " + val);
		}
	}
	
	public void QDRO_Explanation() throws Exception
	{
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		ReusableLibraries.Click("partiallinktext", "What is a QDRO");
		
		ReusableLibraries.sync(High_Wait);
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		String txtVal = ReusableLibraries.GetValue("xpath", "//*[@class='content-item content-label item-3 flex flex-row explanation_body_dataLabelRead centered dataLabelRead explanation_body_dataLabelRead']", "innerHTML");
		String ExpVal1 = "A Qualified Domestic Relations Order, or QDRO is a special court order that grants your former spouse a right to a portion of the retirement benefits you�ve earned through participation in your employer-sponsored retirement plan.";
		String ExpVal2 = "QDROs are typically prepared during divorce proceedings, although they can still be filed years after a divorce.";
		String ExpVal3 = "In a QDRO, you are called the participant. The person who is designated to receive a share of your retirement benefit is called the alternate payee.";
		String ExpVal4 = "QDROs can award retirement benefits to your alternate payee while you�re still alive, as well as survivor benefits in the event you pass away.";
		
		txtVal = txtVal.replaceAll("�", "").replaceAll("�","");
		if(txtVal.contains(ExpVal1) && txtVal.contains(ExpVal2) && txtVal.contains(ExpVal1) && txtVal.contains(ExpVal2))
		{
			htmlReports.reportPASS("QDRO Explanation is as expected");
			System.out.println("Pass");
		}
		else
		{
			htmlReports.reportFAIL("QDRO Explanation is not as expected");
			System.out.println("Fail");
		}
		htmlReports.addScreenshot(currFolder +"\\QDROExplanation" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector", ".icon-btn-float-right > span:nth-child(1) > button:nth-child(1)");
	}
	
	
	public void RMD_Next() throws Exception
	{
		htmlReports.reportINFO("------- In RMD Screen----");
		ReusableLibraries.sync(High_Wait);
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Required Minimum Distribution" + "')]"));
		if(list.size() > 0)
		{
			String txtVal = ReusableLibraries.GetValue("xpath", "//*[@class='content-item content-label item-1 remove-top-spacing remove-left-spacing flex flex-row center dataLabelRead explanation_body_dataLabelRead']", "innerHTML");
			String ExpVal1 = "Based upon your age and federal regulations, your distribution request may be processed as a Required Minimum Distribution (RMD).";
			String ExpVal2 = "The RMD will have a 10% federal tax withholding and any mandatory state tax withholding.";
			
			if(txtVal.contains(ExpVal1) && txtVal.contains(ExpVal2))
			{
				htmlReports.reportPASS("RMD Text is as expected");
				htmlReports.reportINFO("The Text is " + txtVal);
			}
			else
			{
				htmlReports.reportFAIL("RMD Text is not as expected");
				htmlReports.reportINFO("Actual Text is " + txtVal);
				htmlReports.reportINFO("Exp Text is " + ExpVal1 + " " + ExpVal2);
			}
			
			//RMD Explanation----------
			RMD_Explanation();
			
			//---------------------------
			htmlReports.addScreenshot(currFolder +"\\RMD" + currIteration + ".jpg" );
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
	}
	
	
	public void RMD_Explanation() throws Exception
	{
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		ReusableLibraries.Click("partiallinktext", "What is an RMD");
		
		ReusableLibraries.sync(High_Wait);
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		String txtVal = ReusableLibraries.GetValue("xpath", "//*[@class='content-item content-label item-3 flex flex-row explanation_body_dataLabelRead centered dataLabelRead explanation_body_dataLabelRead']", "innerHTML");
		String ExpVal1 = "Required Minimum Distributions (RMDs) generally are minimum amounts that a retirement plan account owner must withdraw � and pay taxes on- annually. Retirement plan participants and IRA owners are responsible for taking the correct amount of RMDs on time every year from their accounts, and they face stiff penalties for failure to take RMDs.";
		String ExpVal2 = "Your first RMD must be paid by April 1 following the year you reach age 70 � or, if later, the year in which you retire. Subsequent payments must be made by December 31.";
		
		
		if(txtVal.contains(ExpVal1) && txtVal.contains(ExpVal2))
		{
			htmlReports.reportPASS("RMD Explanation is as expected");
			htmlReports.reportINFO("The Text is " + txtVal);
		}
		else
		{
			htmlReports.reportFAIL("RMD Explanation is not as expected");
			htmlReports.reportINFO("Actual Text is " + txtVal);
			htmlReports.reportINFO("Exp Text is " + ExpVal1 + " " + ExpVal2);
		}
		
		htmlReports.addScreenshot(currFolder +"\\RMDExplanation" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector", ".icon-btn-float-right > span:nth-child(1) > button:nth-child(1)");
	}
	
	
	
	public void Spousal_Download() throws Exception
	{
		htmlReports.reportINFO("------- In Spousal Download Screen----");
		ReusableLibraries.sync(High_Wait);
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Plan provisions, spousal consent" + "')]"));
		if(list.size() > 0)
		{
			htmlReports.addScreenshot(currFolder +"\\SpousalDownload" + currIteration + ".jpg" );
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}
		
	}
//	///input[@id='$PpyWorkPage$ppyFileName']
	public void Spousal_Upload() throws Exception
	{
		htmlReports.reportINFO("------- In Spousal Upload Screen----");
		ReusableLibraries.sync(High_Wait);
		driver.switchTo().defaultContent();
		ReusableLibraries.switchFrame();
		
		List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "Please upload the spousal consent" + "')]"));
		if(list.size() > 0)
		{
			//ReusableLibraries.Set("xpath", "//input[@class='inputfile inputfile-btn-large']",System.getProperty("user.dir")+"\\2018HolidaySchedule.pdf");
			//ReusableLibraries.Set("id", "$PpyWorkPage$ppyFileName","C:\\Users\\t003320\\eclipse-workspace\\Disbursement_New\\2018HolidaySchedule.pdf");
			//ReusableLibraries.Click("id", "$PpyWorkPage$ppyFileName");
			//String text = "C:\\Users\\t003320\\eclipse-workspace\\Disbursement_New\\2018HolidaySchedule.pdf";
			String text = parentFolder + "\\2018HolidaySchedule.pdf";
			
			ReusableLibraries.sync(Med_Wait);
			
			Actions act = new Actions(driver);
			act.sendKeys(Keys.TAB).build().perform();
			ReusableLibraries.sync(Med_Wait);
			act.sendKeys(Keys.SPACE).build().perform();
			ReusableLibraries.sync(Med_Wait);
			

		
			ReusableLibraries.sync(Med_Wait);
			
			
			StringSelection stringSelection = new StringSelection(text);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(stringSelection, stringSelection);

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			ReusableLibraries.sync(High_Wait);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
					
			ReusableLibraries.sync(High_Wait);
			htmlReports.addScreenshot(currFolder +"\\uploadSpousalConsent" + currIteration + ".jpg" );
			ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		}	
	}
	
}
