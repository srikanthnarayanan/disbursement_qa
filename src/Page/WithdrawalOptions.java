package Page;

import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class WithdrawalOptions extends BaseTest {

	public void clickNext() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		//ReusableLibraries.Click("cssSelector", ".workarea-view-footer > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1) > button:nth-child(1)");
	}
	
	public void clickFull() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (Full/Partial)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\Withdrawal_Full" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector", ".layout-content-stacked_button_group > div:nth-child(1) > span:nth-child(1) > button:nth-child(1)");
		
		//withdrawal option page - what do you want to do with your money
		ReusableLibraries.sync(High_Wait);
		FullWD_TextCheck();
		htmlReports.addScreenshot(currFolder + "\\withdrawalOption1_" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		
		//withdrawal option page
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\withdrawalOption2_" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
		
	}

	
	public void FullWD_TextCheck() throws Exception
	{
		ReusableLibraries.switchFrame();
		String ActText = ReusableLibraries.GetValue("xpath", "//*[@class='content-item content-label item-1 remove-top-spacing remove-left-spacing flex flex-row center dataLabelRead explanation_body_dataLabelRead']", "innerHTML");
		String ExpText = "<p>What do you want to do with your money ?\n" + 
				"<br><br>\n" + 
				"Distributions from  your plan may be paid in the form of annuity. \n" + 
				"<br><br>\n" + 
				"If you would like an annuity distribution, please <br> contact our customer care concierge services at <br><b>1-800-348-6229</b>, option <b>1</b></p>";
		
		if(ExpText.equalsIgnoreCase(ActText))
		{
			htmlReports.reportPASS("FullWD Text is displayed as expected. The Value is "  + ExpText);
		}
		else
		{
			htmlReports.reportFAIL("FullWD Text is not displayed as expected. The Exp Value is "  + ExpText + " and the Act value is " + ActText);
		}
	}
	
	
	public void clickPartial() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (Full/Partial)----");
		
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\Withdrawal_Partial" + currIteration + ".jpg" );
		ReusableLibraries.Click("cssSelector", "div.dataValueWrite:nth-child(2) > span:nth-child(1) > button:nth-child(1)");
	}
	
	public void selectRollOver() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (RollOver/Cash)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\RollOver" + currIteration + ".jpg" );
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-2.flex.flex-row > div > div > div > div.content-item.content-field.item-1.remove-top-spacing.remove-left-spacing.flex.flex-row.dataValueWrite > span > button");
		ReusableLibraries.Click("xpath","//*[@class='content-item content-field item-1 remove-top-spacing remove-left-spacing flex flex-row dataValueWrite']//button[@class='Main_Content_Primary_Button_full_width pzhc pzbutton']");
	}
	
	public void selectCash() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (RollOver/Cash)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\Cash" + currIteration + ".jpg" );
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-2.flex.flex-row > div > div > div > div.content-item.content-field.item-2.flex.flex-row.dataValueWrite > span > button");
		ReusableLibraries.Click("xpath","//*[@class='content-item content-field item-2 flex flex-row dataValueWrite']//button[@class='Main_Content_Primary_Button_full_width pzhc pzbutton']");
	}
	
	public void selectInternalRollOver() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (InternalRollOver/External RollOver)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\InternalRollOver" + currIteration + ".jpg" );
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-field.item-1.flex.flex-row.radio-btn-withdrawal.dataValueWrite > div > div:nth-child(1) > span > label");
		ReusableLibraries.Click("xpath","//label[@for='PayeeTypeInternalRollover']");
		ReusableLibraries.sync(High_Wait);
		//Enter Account Number
		ReusableLibraries.Set("xpath", "//input[@data-test-id='20180919104455049510859']", "1111111");
		//Next
		htmlReports.addScreenshot(currFolder +"\\selectInternalRollOver" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
	}
	
	public void selectCheck() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (Check/ACH)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\InternalRollOver" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//label[@for='PayeeTypeCheckWithdrawal']");

		//Next
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-3.column-3.remove-bottom-spacing.remove-right-spacing > div > div > div > div > div > div > span > button");
		htmlReports.addScreenshot(currFolder +"\\selectCheck" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
	}
	
	public void selectACH() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (Check/ACH)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\InternalRollOver" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//label[@for='PayeeTypeCashWithdrawal']");
		//Enter Details
		ReusableLibraries.Set("id", "pyName", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "PayeeName"));
		ReusableLibraries.Set("id", "AccountType", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "AccountType"));
		ReusableLibraries.Set("id", "pyCity", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "City"));
		ReusableLibraries.Set("id", "pyState", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "State"));
		ReusableLibraries.Set("id", "pyPostalCode", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "PostalCode"));
		ReusableLibraries.Set("id", "ABANumber", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "ABANumber"));
		ReusableLibraries.Set("id", "AccountNumber", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "AccountNumber"));
		//Next
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-3.column-3.remove-bottom-spacing.remove-right-spacing > div > div > div > div > div > div > span > button");
		htmlReports.addScreenshot(currFolder +"\\selectACH" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
	}
	
	public void selectExternalRollOver() throws Exception
	{
		htmlReports.reportINFO("------- In Withdrawal Options Screen (InternalRollOver/External RollOver)----");
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\ExternalRollOver" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//label[@for='PayeeTypeExternalRollover']");
		ReusableLibraries.sync(High_Wait);
		//Enter Details
		ReusableLibraries.Set("id", "pyName", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "PayeeName"));
		ReusableLibraries.Set("id", "AccountType", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "AccountType"));
		ReusableLibraries.Set("id", "AccountNumber", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "AccountNumber"));
		ReusableLibraries.Set("id", "pyStreet", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "Street"));
		ReusableLibraries.Set("id", "pyCity", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "City"));
		ReusableLibraries.Set("id", "pyState", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "State"));
		ReusableLibraries.Set("id", "pyPostalCode", testData.getstringcellvalue("CreateCase", "CreateCase", "Iteration", currIteration, "PostalCode"));
		//Next
		//ReusableLibraries.Click("cssSelector", "#RULE_KEY > div > div > div > div.content-item.content-layout.item-3.column-3.remove-bottom-spacing.remove-right-spacing > div > div > div > div > div > div > span > button");
		//ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		htmlReports.addScreenshot(currFolder +"\\selectExternalRollOver" + currIteration + ".jpg" );
		ReusableLibraries.Click("xpath", "//button[@class='Primary_Button_Small pzhc pzbutton']");
	}
	
	public void TaxWHScreens() throws Exception
	{
		if(driver.getPageSource().contains("Federal Tax Withholding"))
		{
			ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
		}
		
		if(driver.getPageSource().contains("Would you like to withhold extra in addition"))
		{
			if(testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "additionalTax").equalsIgnoreCase("Yes"))
			{
				ReusableLibraries.Click("cssSelector", "#RULE_KEY > div.layout.layout-none > div > div > div > div > div > div > div.content-item.content-field.item-1.flex.flex-row.dataValueWrite > span > button");
				ReusableLibraries.Button_Click(driver,"button","title","Complete this assignment");
				
			}
			else
			{
				ReusableLibraries.Click("cssSelector","#RULE_KEY > div.layout.layout-none > div > div > div > div > div > div > div.content-item.content-field.item-2.flex.flex-row.dataValueWrite > span > button");
			}
		}
	}
	
	//
	
}
