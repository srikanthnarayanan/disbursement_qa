package Page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Test.BaseTest;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class DistributionOptions extends BaseTest {

	public void selectSFS() throws Exception
	{
		htmlReports.reportINFO("---Select Seperation From Service---");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		WebElement SFS = ReusableLibraries.getElement("cssSelector", "div.content-label:nth-child(2)");
		htmlReports.addScreenshot(currFolder +"\\SFS" + currIteration + ".jpg" );
		ReusableLibraries.Click(SFS);
	}
	
	public void selectIRAD() throws Exception
	{
		htmlReports.reportINFO("---Select IRA Distribution---");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		WebElement SFS = ReusableLibraries.getElement("cssSelector", "div.content-label:nth-child(2)");
		htmlReports.addScreenshot(currFolder +"\\IRAD" + currIteration + ".jpg" );
		ReusableLibraries.Click(SFS);
	}
}
