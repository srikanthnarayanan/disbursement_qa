package Page;

import static org.testng.Assert.assertFalse;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import Test.BaseTest;
import testDataFactory.testData;
import utility.ReusableLibraries;
import utility.htmlReports;
import utility.logs;

public class selectPlan extends BaseTest {

	public void selectPlanId() throws Exception
	{
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.sync(High_Wait);
		//ReusableLibraries.sync(High_Wait);
		//Thread.sleep(10000);
		ReusableLibraries.Click("linktext","Dashboard");  //dashboard opens Inv ID application, hence commenting
		
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		ReusableLibraries.Click("cssselector","div.align-end:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > span:nth-child(1) > button:nth-child(1)");
				
		String partIDVal = testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Participant");
		String planIDVal = testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Plan");
		
		WebElement partID = driver.findElement(By.id("pyWorkPartyUri"));
		ReusableLibraries.waitFor(partID);
		ReusableLibraries.Set(partID, partIDVal);
		
		WebElement planID = driver.findElement(By.id("pyID"));
		ReusableLibraries.waitFor(planID);
		ReusableLibraries.Set(planID, planIDVal + Keys.TAB);
		
		ReusableLibraries.sync(High_Wait);
		htmlReports.addScreenshot(currFolder +"\\PlanDetails" + currIteration + ".jpg" );
		ReusableLibraries.Click("linkText", partIDVal);
	}
	
	public void select_Plan_Id() throws Exception
	{
		startMyDistribution();
		SelectPlanid();
		selectParLink();
	}
	
	public void startMyDistribution() throws Exception
	{
		htmlReports.reportINFO("---Click start my Distribution---");
		ReusableLibraries.sync(High_Wait);
		WebElement Dashboard = driver.findElement(By.xpath("//li[@title='Dashboard']"));
		//Dashboard.click();
		ReusableLibraries.Click(Dashboard);
		
		ReusableLibraries.switchFrame();
		ReusableLibraries.sync(High_Wait);
		WebElement startMyDistribution = driver.findElement(By.xpath("//*[@class='content-item content-field item-1 remove-top-spacing remove-bottom-spacing remove-left-spacing flex flex-row dataValueWrite']//button[@class='pzhc pzbutton']"));
		ReusableLibraries.waitFor(startMyDistribution);
		htmlReports.addScreenshot(currFolder +"\\startMyDistribution" + currIteration + ".jpg" );
		ReusableLibraries.Click(startMyDistribution);	
	}
	
	public void SelectPlanid() throws Exception
	{
		htmlReports.reportINFO("---Enter Participant and Plan id---");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		WebElement txtParticipantID = driver.findElement(By.xpath("//input[@data-test-id='2018062710122502511185']"));
		WebElement txtPlanNum = driver.findElement(By.xpath("//input[@data-test-id='2018062710122502522420']"));
		
		String partIDVal = testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Participant");
		String planIDVal = testData.getstringcellvalue("CreateDistribution", currTest, "Iteration", currIteration, "Plan");
		
		ReusableLibraries.waitFor(txtParticipantID);
		ReusableLibraries.Set(txtParticipantID, partIDVal);
		ReusableLibraries.Set(txtPlanNum, planIDVal);	
	}
	
	public String SelectPlanid(String Participant, String PlanNum) throws Exception
	{
		try
		{
			htmlReports.reportINFO("---Enter Participant and Plan id---");
			ReusableLibraries.sync(High_Wait);
			ReusableLibraries.switchFrame();
			WebElement txtParticipantID = driver.findElement(By.xpath("//input[@data-test-id='2018062710122502511185']"));
			WebElement txtPlanNum = driver.findElement(By.xpath("//input[@data-test-id='2018062710122502522420']"));
			
			String partIDVal = Participant;
			String planIDVal = PlanNum;
			
			ReusableLibraries.waitFor(txtParticipantID);
			ReusableLibraries.Set(txtParticipantID, partIDVal);
			ReusableLibraries.Set(txtPlanNum, planIDVal);
		}
		catch(NoSuchElementException nse)
		{
			return "Fail " + nse.getMessage();
		}
	
	return "Pass";
		
	}
	
	
	public String selectParLink() throws Exception
	{
		try {
				htmlReports.reportINFO("---Click the Participant fetched---");
				ReusableLibraries.sync(High_Wait);
				ReusableLibraries.switchFrame();
				WebElement lnkPartLink = driver.findElement(By.xpath("//a[@data-test-id='201810301645580178510']"));
				ReusableLibraries.waitFor(lnkPartLink);
				htmlReports.addScreenshot(currFolder +"\\selectParticipantLink" + currIteration + ".jpg" );
				lnkPartLink.click();
			}
		catch(NoSuchElementException nse)
			{
				return "Fail " + nse.getMessage();
			}
		
		return "Pass";
	}
}
