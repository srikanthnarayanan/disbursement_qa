package utility;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class logs 
{
public static void log(String logger)
{
	Logger log = Logger.getLogger("sample");
	PropertyConfigurator.configure("log4j.properties");
	log.info(logger);
}
}
