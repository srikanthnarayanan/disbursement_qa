package utility;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import testDataFactory.testData;

public class browserSelection 
{
	static testData tData = new testData();
	public static WebDriver driver;

		@SuppressWarnings("deprecation")
		public static WebDriver Browser(String testCaseName) throws Exception
		{
				if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("firefox"))
				{	
					System.setProperty("webdriver.gecko.driver", utility.variableDeclarations.parentFolder + "\\Driver\\geckodriver.exe");
					driver = new FirefoxDriver();
					
					/*FirefoxOptions fopt = new FirefoxOptions();
					fopt.setCapability("browser.download.folderList",2);
					fopt.setCapability("browser.download.manager.showWhenStarting",false);
					fopt.setCapability("browser.download.dir","c:\\mydownloads");
					fopt.setCapability("browser.helperApps.neverAsk.saveToDisk","application/xlsx;text/csv");
					
					driver = new FirefoxDriver(fopt);
					*/
			    }
				
				else if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("chrome"))
				{
					System.setProperty("webdriver.chrome.driver", utility.variableDeclarations.parentFolder + "\\Driver\\chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new ChromeDriver(capabilities);
				}
				else if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("ie"))
				{		
					System.setProperty("webdriver.ie.driver", utility.variableDeclarations.parentFolder + "\\Driver\\IEDriverServer.exe");			
					DesiredCapabilities caps= DesiredCapabilities.internetExplorer();
					//caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
										
					driver = new InternetExplorerDriver(caps);
				}
					driver.get(tData.getstringcellvalue("Login",testCaseName,"URL_SSO"));
					//driver.get(tData.getstringcellvalue("Login",testCaseName,"NAVIGATOR_URL"));
				return driver;
		}		
		
		public static WebDriver BrowserProfileChange(String testCaseName, String PDFFolderPath) throws Exception
		{
				if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("firefox"))
				{	 
					//DesiredCapabilities dc = DesiredCapabilities.firefox();
					//dc.merge(capabillities);
					FirefoxProfile profile = new FirefoxProfile();
				    //String path = "C:\\Workspace\\Navigator\\Utility\\PDF";
				    profile.setPreference("browser.helperApps.neverAsk.openFile", "application/pdf");
				    profile.setPreference("browser.download.folderList", 2);
				    profile.setPreference("browser.download.dir", PDFFolderPath);
				    profile.setPreference("browser.download.alertOnEXEOpen", false);
				    profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/x-msexcel,application/excel,application/x-excel,application/excel,application/x-excel,application/excel,application/vnd.ms-excel,application/x-excel,application/x-msexcel,application/octet-stream,application/pdf");
				    profile.setPreference("browser.download.manager.showWhenStarting", false);
				    profile.setPreference("browser.download.manager.focusWhenStarting", false);
				    profile.setPreference("browser.helperApps.alwaysAsk.force", false);
				    profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
				    profile.setPreference("browser.download.manager.closeWhenDone", false);
				    profile.setPreference("browser.download.manager.showAlertOnComplete", false);
				    profile.setPreference("browser.download.manager.useWindow", false);
				    profile.setPreference("browser.download.manager.showWhenStarting", false);
				    profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
				    profile.setPreference("pdfjs.disabled", true);
				    FirefoxOptions options = new FirefoxOptions();
				    options.setProfile(profile);
 			 	   driver = new FirefoxDriver(); 
				}
				else if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("chrome"))
				{
					System.setProperty("webdriver.chrome.driver", "C:\\GTP_ProjectWorkSpace\\Driver\\chromedriver.exe");
					driver = new ChromeDriver();
				}
				else if(tData.getstringcellvalue("Login",testCaseName,"Browser").equals("ie"))
				{
					System.setProperty("webdriver.ie.driver", "C:\\WorkSpace\\Driver\\IEDriverServer.exe");
					driver = new InternetExplorerDriver();
				}
					driver.get(tData.getstringcellvalue("Login",testCaseName,"URL_SSO"));
					//driver.get(tData.getstringcellvalue("Login",testCaseName,"NAVIGATOR_URL"));
				return driver;
		}
		
		public static void SelectlastFrame(WebDriver driver) throws Exception
		{
			List <WebElement> Frames = driver.findElements(By.tagName("iframe"));
			for(WebElement frame:Frames)
			{
				System.out.println(frame.getAttribute("name"));
			}
			WebElement frame1 = Frames.get(Frames.size()-1);
			driver.switchTo().frame(frame1.getAttribute("name"));
		}
	
}
