package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import utility.Enums;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

@SuppressWarnings("deprecation")
public class WebServices{
		
	public static JSONObject retrieve(String cusip) throws IOException, IOException, AuthenticationException
	{
		  String line;
		  HttpClient client = new DefaultHttpClient();
		  HttpGet request = new HttpGet("https://stgesb.oneamerica.com:61018/investmentapi/investments/morningstar/funds?cusip=" + cusip);
		  request.addHeader("client_id", "fa74ee11ef9b42d184abecf54b2e5577");
		  request.addHeader("client_secret", "eBE0a092434f471691513302567FFE94");
		/*  UsernamePasswordCredentials creds =  new UsernamePasswordCredentials("John", "pass");
		  request.addHeader(new BasicScheme().authenticate(creds, request, null));
		  */
		  HttpResponse response = client.execute(request);
		 
		  
		  //System.out.println(response.getEntity()
		  String jsonString = EntityUtils.toString(response.getEntity());
		  //System.out.println(jsonString);
		  
		  int i = jsonString.indexOf("{");
		  jsonString = jsonString.substring(i);
		  JSONObject obj = new JSONObject(jsonString.trim()); 
		  //System.out.println(json.toString(4)); 
		  
		  return obj;
		  
		 //System.out.println(obj.getString("closedToAllInvestors"));

		  
		/*BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
		  while ((line = rd.readLine()) != null) {
		    System.out.println(line);
		  }*/
		  
		  
	}	
	
	public static boolean compare(JSONObject object, Enums.MstarParam key, String compareval)
	
	{		
		if(object.getString(key.toString()).trim().equalsIgnoreCase(compareval.trim()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void main(String[] args) throws IOException, AuthenticationException
	{
		JSONObject obj1 = retrieve("354723298");
		boolean blncusip = compare(obj1,Enums.MstarParam.cusip,"354723298");
		boolean blncategory = compare(obj1,Enums.MstarParam.category,"Muni National Interm");
	
	}

}