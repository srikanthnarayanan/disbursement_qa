package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class dataBase_Utility {
	
	public static String dbRetrieve(String qry,String databaseColumn) throws ClassNotFoundException, SQLException
	   {
		   	String dbval = "";
		   	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		   	//QA DB
			Connection conn = DriverManager.getConnection("jdbc:sqlserver://" + "SQLT10699:5353" + ";databaseName=" + "RS_OperationalDataStore" +";integratedsecurity=true;");
		   	
		   	//DEV DB
			//Connection conn = DriverManager.getConnection("jdbc:sqlserver://" + "SQLD10699:4242" + ";databaseName=" + "RS_OperationalDataStore" +";integratedsecurity=true;");
			
			// Connection conn = DriverManager.getConnection("jdbc:sqlserver://" + DBServer + ";databaseName=" + databaseName + ";user=T003320;password=;");  //
			 Statement stmt = null;
			// String query = "select PRTCPNT_SSN from ods.PRTCPNT_REF where PRTCPNT_SSN like '" + prtcpnt + "'";
			// String query1 = "select PRTCPNT_ELIG_STAT_RSN_DESCR from ref.PRTCPNT_ELIG_STAT_RSN p where PRTCPNT_ELIG_STAT_RSN_GEN_ID in(select PRTCPNT_ELIG_STAT_RSN_GEN_ID from ods.PRTCPNT_REF where PRTCPNT_SSN like '144787711')";
			 try 
			 {
				 stmt = conn.createStatement();
				 ResultSet rs = stmt.executeQuery(qry);
				 
				 while(rs.next())
				 {
					 dbval = rs.getString(databaseColumn);
					 rs.previous();
					 break;
					 //testData.setstringcellvalue1("Services",TestcaseName, Iteration, "DB_Value", dbval);
				 }
			 }
			 catch(SQLException e)
			 {
				 
			 }
			 catch(NullPointerException npe)
			 {
				// testData.setstringcellvalue1("Services",TestcaseName, Iteration, "DB_Value", "null");
			 }
			 return dbval;
	   }
	
	

}
