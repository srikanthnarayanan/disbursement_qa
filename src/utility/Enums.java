package utility;

public final class Enums {
	public enum DistributionOption {SFS,IRAD;}
	public enum SpousalConsent {Yes,No;}
	public enum RMD	{Yes,No;}
	public enum WithDrawalType {Full,Partial};
	public enum TAXWH {NA,Yes,NO};
	public enum Reservist{Yes,No}
	public enum WithDrawalOption{InternalRollOver,ExternalRollover,Check,ACHEFT};
	
	//////////////////////////////////////////////////////////////////////////////
	public enum decisions
	{
		RTI {
		    @Override
		    public String toString() {
		      return "RTI";
		    }
		  },
		STSAT {
			    @Override
			    public String toString() {
			      return "STSAT";
			    }
			  },
		REJECT {
				    @Override
				    public String toString() {
				      return "REJECT";
				    }
				  },
		SA_STFR {
					    @Override
					    public String toString() {
					      return "SA_STFR";
					    }
				  },
		SA_RTI {
					    @Override
					    public String toString() {
					      return "SA_RTI";
					    }
				  },		  
		Final_Approve {
					    @Override
					    public String toString() {
					      return "Final_Approve";
					    }
				  },
		Final_RTI {
					    @Override
					    public String toString() {
					      return "Final_RTI";
					    }
				  },
		Final_RTSA {
					    @Override
					    public String toString() {
					      return "Final_RTSA";
					    }
				  },
		Final_Reject {
					    @Override
					    public String toString() {
					      return "Final_Reject";
					    }
				  };
	}
	
	public enum workbasket
	{
		DataCheck {
		    @Override
		    public String toString() {
		      return "DataCheck";
		    }
		  },
		SeparateAccount {
			    @Override
			    public String toString() {
			      return "SeparateAccount";
			    }
			  },
		FinalReview {
				    @Override
				    public String toString() {
				      return "FinalReview";
				    }
				  };
	}

	public enum MstarParam
	{
	cusip {
	    @Override
	    public String toString() {
	      return "cusip";
	    }
	  },
	category {
		    @Override
		    public String toString() {
		      return "category";
		    }
		  },
	redemptionFeeIndicator {
			    @Override
			    public String toString() {
			      return "redemptionFeeIndicator";
			    }
			  },
	redemptionFeePercentage {
				    @Override
				    public String toString() {
				      return "redemptionFeePercentage";
				    }
				  },
	fundInceptionDate {
					    @Override
					    public String toString() {
					      return "fundInceptionDate";
					    }
					  },
	netExpenseRatio {
	    @Override
	    public String toString() {
	      return "netExpenseRatio";
	    }
		},
	grossExpenseRatio {
	    @Override
	    public String toString() {
	      return "grossExpenseRatio";
	    }
		},
	ticker {
	    @Override
	    public String toString() {
	      return "ticker";
	    }
		},
	prospectusName {
	    @Override
	    public String toString() {
	      return "prospectusName";
	    }
	  },
	prospectusObjective {
	    @Override
	    public String toString() {
	      return "prospectusObjective";
	    }
	  },
	secId {
	    @Override
	    public String toString() {
	      return "secId";
	    }
	  },
	morningstarName {
	    @Override
	    public String toString() {
	      return "morningstarName";
	    }
	  },
	morningstar3yrReturn {
	    @Override
	    public String toString() {
	      return "morningstar3yrReturn";
	    }
	  },
	morningstarRisk {
	    @Override
	    public String toString() {
	      return "morningstarRisk";
	    }
	  },
	shareClassType {
	    @Override
	    public String toString() {
	      return "shareClassType";
	    }
	  },
	prospectusExpenseWaiverType {
	    @Override
	    public String toString() {
	      return "prospectusExpenseWaiverType";
	    }
	  },
	grossExpenseWaiverExpirationDate {
	    @Override
	    public String toString() {
	      return "grossExpenseWaiverExpirationDate";
	    }
	  },
closedToAllInvestorsDate {
	    @Override
	    public String toString() {
	      return "closedToAllInvestorsDate";
		    }
		  },
	closedToAllInvestors {
	    @Override
	    public String toString() {
	      return "closedToAllInvestors";
	    }
	  },
	closedToNewInvestorsDate {
	    @Override
	    public String toString() {
	      return "closedToNewInvestorsDate";
	    }
	  },
	closedToNewInvestors {
	    @Override
	    public String toString() {
	      return "closedToNewInvestors";
	    }
	  };
	}


	public enum colnames
	{
		CUSIP {
		    @Override
		    public String toString() {
		      return "CUSIPID";
		    }
		  },
		
		MorningStarName {
		    @Override
		    public String toString() {
		      return "MorningStarName";
		    }
		  },
		
		MorningStarCategory {
			    @Override
			    public String toString() {
			      return "Category";
			    }
			  },
		
		RedemptionFeeIndicator {
			    @Override
			    public String toString() {
			      return "RFIndicator";
			    }
				  },
		
		RedemptionFeePercentage {
		    @Override
		    public String toString() {
		      return "RFPercentage1";
		    }
		  },
		
		FundInceptionDate1 {
		    @Override
		    public String toString() {
		      return "FundInceptionDate";
		    }
		  },
		
		NetExpRatio {
	    @Override
	    public String toString() {
	      return "NetExpRatio";
	    }
			  },
		
		GrossExpRatio {
	    @Override
	    public String toString() {
	      return "GrsExpRatio";
	    }
				  },
		
		Ticker {
		    @Override
		    public String toString() {
		      return "Ticker";
		    }
					  },
		
	ProspectusName {
	    @Override
	    public String toString() {
	      return "ProspectusName";
	    }
	  },
	
	ProspectusObjective {
	    @Override
	    public String toString() {
	      return "ProspectusObjective";
	    }
		  },
	
	SECID {
			    @Override
			    public String toString() {
			      return "SECID";
			    }
				  },
			  
		  
		Ticker1 {
		    @Override
		    public String toString() {
		      return "Ticker1";
		    }
		  };
}
}
