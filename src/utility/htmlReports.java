package utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.HTMLReporter;
import com.relevantcodes.extentreports.LogStatus;

import Test.BaseTest;
import Test.Config;
//import testDataFactory.testData;

public class htmlReports extends BaseTest 
{
	
	public static ExtentReports Report;
    public static ExtentTest ReportLogger;
        
    
    
    @SuppressWarnings("deprecation")
	public static void reportSTART(String testDetails) throws Exception
    {  
    	CreateFolder(testDetails);
    	String dateFormat = new SimpleDateFormat("MM-dd-yyyy hh-mm-ss").format(new Date());
    	//Report= new ExtentReports("C:\\Workspace\\Navigator\\Utility\\TestReport_"+dateFormat+".html", false);
    	Report= new ExtentReports(currFolder +"\\TestRun.html",false);
    	//ReportLogger = Report.startTest(testDetails);
    	//Report.config().addCustomStylesheet(parentFolder + "\\src\\utility\\Extent_Config.css");
    }
    
    
    public static void startTest(String testDetails) throws Exception
    {
    	ReportLogger = Report.startTest(testDetails);
    }
    
    @SuppressWarnings("deprecation")
	public static void reportSTARTNew(String testDetails) throws Exception
    {  
    	CreateFolderNew(testDetails);
    	String dateFormat = new SimpleDateFormat("MM-dd-yyyy hh-mm-ss").format(new Date());
    	//Report= new ExtentReports("C:\\Workspace\\Navigator\\Utility\\TestReport_"+dateFormat+".html", false);
    	Report= new ExtentReports(currFolder +"\\TestRun.html",false);
    	ReportLogger = Report.startTest(testDetails);
    	//Report.config().addCustomStylesheet(parentFolder + "\\src\\utility\\Extent_Config.css");
    }
    
    public static void endTest() throws Exception
    {
    	Report.endTest(ReportLogger);
    }
    
    public static void reportEND()
    {    
    	//Report.endTest(ReportLogger);
   	    Report.flush();
   	    Report.close();
    }

     public static void reportPASS(String passDetails)
    {    
    	ReportLogger.log(LogStatus.PASS, passDetails);	
    	 logs.log(passDetails);
    }
     
     public static void reportFAIL(String failDetails)
     {    
     	 ReportLogger.log(LogStatus.FAIL, failDetails);	
     	 logs.log(failDetails);
     }
     
     public static void reportWARNING(String warningDetails)
     {    
     	 ReportLogger.log(LogStatus.WARNING, warningDetails);	
     	 logs.log(warningDetails);
     }
     
     public static void reportINFO(String infoDetails)
     {    
    	 
     	ReportLogger.log(LogStatus.INFO, infoDetails);	
     	logs.log(infoDetails);
     }
     
     public static void addScreenshot(String screenshotpath) throws Exception
     {  
    	Screenshot(screenshotpath); 
    	ReportLogger.log(LogStatus.INFO, ReportLogger.addScreenCapture(screenshotpath));
     }

     public static void CreateFolder(String testDetails) throws Exception
     {
     	//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
     	SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy_HHmmss"); 
     	Date date = new Date();
     	String timeStamp = formatter.format(date);
     	currFolder = parentFolder + "\\Utility\\" + testDetails + "_" + timeStamp;
     	new File(currFolder).mkdirs();
     	System.setProperty("currentfolder", currFolder);
     }
     
     public static void CreateFolderNew(String testDetails) throws Exception
     {
     	//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
     	SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy_HHmmss"); 
     	Date date = new Date();
     	String timeStamp = formatter.format(date);
     	newFolder = currFolder;
     	currFolder = currFolder + "\\" + testDetails;
     	new File(newFolder).mkdirs();
     	
     }
     
     public static void Screenshot(String fileName) throws Exception
     {
    	 File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	 // Now you can do whatever you need to do with it, for example copy somewhere
    	 FileUtils.copyFile(scrFile, new File(fileName));
     }

	
	
}