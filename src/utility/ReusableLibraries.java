package utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import Test.BaseTest;



public class ReusableLibraries extends BaseTest {
	
	static WebDriver driver;
	public static WebElement returnElement, element;
	
	public ReusableLibraries(WebDriver driver)
	{
		this.driver = driver;
	}
	

	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public static void Set(String Prop, String Val1, String Value) throws InterruptedException
	{
		if(!Value.isEmpty() && Value !=null)
		{
			try
			{
				element = getElement(Prop,Val1);
				waitFor(element);
				//sync(High_Wait);
				element.sendKeys(Value + Keys.TAB);
				//htmlReports.reportINFO(Value + " entered in the Edit box " + element.getAttribute("name"));
				//Reporter.log("Pass");
				htmlReports.reportINFO(Value + " entered in the Edit box " + element.getAttribute("innerHTML"));
			}
			catch(NoSuchElementException e)
			{
				Thread.sleep(High_Wait);
				Set(Prop,Val1,Value);
			}
			catch(StaleElementReferenceException seE)
			{
				Thread.sleep(High_Wait);
				Set(Prop,Val1,Value);
			}
			catch(NullPointerException npe)
			{
				Thread.sleep(High_Wait);
				Set(Prop,Val1,Value);
			}
		}
	}
	
	public static void robot_Text(String text) throws Exception
	{
		StringSelection stringSelection = new StringSelection(text);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
	}
	
	public static String Get(String Prop, String Val1) throws InterruptedException
	{
		String returnVal = "";
			try
			{
				element = getElement(Prop,Val1);
				waitFor(element);
				//sync(High_Wait);
				returnVal = element.getAttribute("value");
				htmlReports.reportINFO(returnVal + " captured from the Edit box " + element.getAttribute("innerHTML"));
				Reporter.log("Pass");
			}
			catch(NoSuchElementException e)
			{
				Thread.sleep(High_Wait);
				Get(Prop,Val1);
			}
			catch(StaleElementReferenceException seE)
			{
				Thread.sleep(High_Wait);
				Get(Prop,Val1);
			}
			catch(NullPointerException npe)
			{
				Thread.sleep(High_Wait);
				Get(Prop,Val1);
			}
			
			return returnVal;
	}
	
	public static String GetValue(String Prop, String Val1, String fetchprop) throws InterruptedException
	{
			String returnVal = "";
			try
			{
				element = getElement(Prop,Val1);
				waitFor(element);
				//sync(High_Wait);
				returnVal = "" + element.getAttribute(fetchprop);
			//htmlReports.reportINFO(returnVal + " captured from the Element");
				//Reporter.log("Pass");
			}
			catch(NoSuchElementException e)
			{
				Thread.sleep(High_Wait);
				GetValue(Prop,Val1,fetchprop);
			}
			catch(StaleElementReferenceException seE)
			{
				Thread.sleep(High_Wait);
				GetValue(Prop,Val1,fetchprop);
			}
			catch(NullPointerException npe)
			{
				Thread.sleep(High_Wait);
				GetValue(Prop,Val1,fetchprop);
			}
			
			return returnVal;
	}
	
	
	public static void Click(String Prop, String Val1) throws InterruptedException
	{
		try
		{
			element = getElement(Prop,Val1);
			waitFor(element);
			//htmlReports.reportINFO(element.getAttribute("name") + " Clicked as Expected");
			if(!(element.getAttribute("innerHTML").contains("<")))
			{
				htmlReports.reportINFO(element.getAttribute("innerHTML") + " Clicked as Expected");
			}
			else if(element.getAttribute("title").isEmpty() || element.getAttribute("title") == "")
			{
				htmlReports.reportINFO(element.getText() + " Clicked as Expected");
			}
			else
			{
				htmlReports.reportINFO(element.getAttribute("title") + " Clicked as Expected");
			}
			element.click();
			//Reporter.log("Pass");
		}
		catch(NoSuchElementException e)
		{
			Thread.sleep(High_Wait);
			Click(Prop,Val1);
		}
		catch(StaleElementReferenceException seE)
		{
			Thread.sleep(High_Wait);
			Click(Prop,Val1);
		}
		catch(NullPointerException npe)
		{
			Thread.sleep(High_Wait);
			Click(Prop,Val1);
		}
		catch(ElementNotInteractableException eni)
		{
			Thread.sleep(High_Wait);
			Click(Prop,Val1);
		}
	}
	
	public static void Select(String Prop, String Val1, String Value) throws InterruptedException
	{
		if(!Value.isEmpty() && Value !=null)
		{
			try 
			{
				org.openqa.selenium.support.ui.Select element = new org.openqa.selenium.support.ui.Select(getElement(Prop,Val1));
				//waitFor(element);
				//htmlReports.reportINFO("Selected " + Value + " as Expected");
				htmlReports.reportINFO("Selected " + Value + " as Expected");
				element.selectByVisibleText(Value);
				Reporter.log("Pass");
			}
			catch(NoSuchElementException e)
			{
				Thread.sleep(High_Wait);
				Select(Prop,Val1,Value);
			}
			catch(StaleElementReferenceException seE)
			{
				Thread.sleep(High_Wait);
				Select(Prop,Val1,Value);
			}
			catch(NullPointerException npe)
			{
				Thread.sleep(High_Wait);
				//Select(Prop,Val1,Value);
			}
			catch(ElementNotInteractableException eni)
			{
				Thread.sleep(High_Wait);
				//Select(Prop,Val1,Value);
			}
		}
		
	}
	
	
	
	public static void waitFor(WebElement element) throws InterruptedException
	{
		try 
		{
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.pollingEvery(1000, TimeUnit.MILLISECONDS);
		wait.until(ExpectedConditions.visibilityOf(element));
		}
		catch(Exception e)
		{
			sync(High_Wait);
		}
	}
	
	public static void switchFrame()
	{
		try
		{
			driver.switchTo().defaultContent();
			List <WebElement> Frames = driver.findElements(By.tagName("iframe"));
			WebElement frame1 = Frames.get(Frames.size()-1);
			//driver.switchTo().frame(frame1.getAttribute("name"));
			WebDriverWait wait = new WebDriverWait(driver,20);
			String FrameName = frame1.getAttribute("name");
			wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(FrameName));
			//htmlReports.reportINFO("Switched to " + FrameName + " frame as expected");
			//logs.log("Switched to " + FrameName + " frame as expected");
		}
		catch(StaleElementReferenceException seE)
		{
			switchFrame();
		}
	}	
	
	public static void switchDefaultFrame()
	{
		driver.switchTo().defaultContent();
	}	
	
		
	public static void sync(int waittime) throws InterruptedException
	{
		Thread.sleep(waittime);
		
	}
	
	
	public static WebElement getElement(String Prop, String Val1)
	{
		WebElement element = null;
		try
		{
			if(Prop.equalsIgnoreCase("id"))
			{
				element = driver.findElement(By.id(Val1));
			}
			else if(Prop.equalsIgnoreCase("name"))
			{
				element = driver.findElement(By.name(Val1));
			}
			else if(Prop.equalsIgnoreCase("xpath"))
			{
				element = driver.findElement(By.xpath(Val1));
			}
			else if(Prop.equalsIgnoreCase("linktext"))
			{
				element = driver.findElement(By.linkText(Val1));
			}
			else if(Prop.equalsIgnoreCase("partiallinktext"))
			{
				element = driver.findElement(By.partialLinkText(Val1));
			}
			else if(Prop.equalsIgnoreCase("cssselector"))
			{
				element = driver.findElement(By.cssSelector(Val1));
			}
		}
		catch(Exception e)
		{
			
		}
		return element;
	}

	
	public static void SelectCase(String Case_no) throws Exception
	{
		int itmfnd = 0;
		Thread.sleep(High_Wait);
		
		do {
			try {
					WebElement case1 = driver.findElement(By.linkText(Case_no));
					waitFor(case1);
					case1.click();	
					htmlReports.reportINFO("Selected Case ID " + Case_no);
					itmfnd = 1;
				}
			catch(NoSuchElementException e)
				{
					Thread.sleep(Low_Wait);
					WebElement btnnext = driver.findElement(By.xpath("//*[@id=\"RULE_KEY\"]/div[1]/div/div/div[4]/div/div/span/button"));
					Thread.sleep(Low_Wait);
					if(btnnext.isEnabled())
					{
						btnnext.click();
					}
					else
					{
						itmfnd = 1;
						//screenshot.takeSnapShot(driver, ScreenshotPath+"CasenotFound.png");
						htmlReports.reportFAIL("Reached End of Page... can't find the Case ID");
					}
				}
			} while(itmfnd == 0);
	}
	
	public static String FindFile(String pathToScan, String Caseno)
	{
		       //String pathToScan = ".";
		        String target_file = "";  // fileThatYouWantToFilter
		        File folderToScan = new File(pathToScan); 



		    File[] listOfFiles = folderToScan.listFiles();

		     for (int i = 0; i < listOfFiles.length; i++) 
		     {
		            if (listOfFiles[i].isFile()) 
		            {
		                target_file = listOfFiles[i].getName();
		                if(target_file.contains(Caseno))
		                {
		                	break; 
		                }
		                
		           }
		     }    
	
		     return target_file;
	}
	
	
	public static WebElement selectObjectsbyTag(String tagname, String strproperty, String strValue)
	{
		try {
				List<WebElement> allElements = driver.findElements(By.tagName(tagname));
				int itmsfnd = 0;
				for(WebElement element: allElements)
				{
					if(element.getAttribute(strproperty) != null)
					{
					if(element.getAttribute(strproperty).contains(strValue))
						{
									returnElement = element;
									itmsfnd = 1;
									break;
							
						}
					}
				}
				if(itmsfnd == 0)
					{
						return null;
					}
				else
					{
						return returnElement;
					}
		}
		catch(StaleElementReferenceException SER)
		{
			 selectObjectsbyTag(tagname, strproperty, strValue);
		}
		
		return returnElement;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	public static boolean Button_Click(WebDriver driver, String tagname, String strproperty, String strValue) throws Exception
	{
		
		Thread.sleep(1000);
		boolean result = true;
		try {		
				List<WebElement> allElements = driver.findElements(By.tagName(tagname));
				int itmsfnd = 0;
				for(WebElement element: allElements)
				{
					if(element.getAttribute(strproperty) != null)
					{
						if(element.getAttribute(strproperty).contains(strValue))
							{
								//returnElement = element;
								//ReusableLibraries.waitForElement(driver, element);
								//new WebDriverWait(driver,120).until(ExpectedConditions.elementToBeClickable(element));
								
								try {
										if(element.isDisplayed())
										{
											result = true;
											new WebDriverWait(driver,30).until(ExpectedConditions.visibilityOf(element));
											htmlReports.reportINFO(element.getAttribute("name") + " Button is Present & is clicked");
											element.click();
											
											itmsfnd = 1;
											break;
										}
									}
								catch(NoSuchElementException nse)
								{
									result = false;
									htmlReports.reportINFO(" Button is not Present");
								}
								
								
								
							}
					}
				}
				allElements.clear();
			}
		catch(StaleElementReferenceException seE)
		{
			Button_Click(driver,tagname,strproperty,strValue);
		}
		return result;
	}
	
	
	
	
	
	
	
	
	
	
	public static WebElement selectObjects(WebDriver driver, String tagname, String strproperty, String strValue)
	{
		//WebElement returnElement;
		try {
		List<WebElement> allElements = driver.findElements(By.tagName(tagname));
		int itmsfnd = 0;
		for(WebElement element: allElements)
		{
			if(element.getAttribute(strproperty) != null)
			{
			if(element.getAttribute(strproperty).contains(strValue))
				{
										
					try {

								//result = true;
								returnElement = element;
								itmsfnd = 1;
								break;
					}
					catch(NoSuchElementException nse)
					{
						
					}
					
				}
			}
		}
		if(itmsfnd == 0)
			{
			  returnElement = null;
			}
		else
			{
				//return returnElement;
			}
		}
		catch(StaleElementReferenceException seE)
		{
			selectObjects(driver,tagname,strproperty,strValue);
		}
		return returnElement;
	}
	
	
	
	public static void Set(WebElement element, String Value) throws InterruptedException
	{
		try
		{
			
			element.sendKeys(Value);
			//htmlReports.reportINFO(Value + " entered in the Edit box " + element.getAttribute("name"));
			Reporter.log("Pass");
		}
		catch(NoSuchElementException e)
		{
			//Set(element,Value);
		}
		catch(StaleElementReferenceException seE)
		{
			//Set(element,Value);
		}
		catch(NullPointerException npe)
		{
			//Set(element,Value);
		}
	}
	
	public static void Click(WebElement element) throws InterruptedException
	{
		try
		{
			
			
			waitFor(element);
			element.click();
			//htmlReports.reportINFO(Value + " entered in the Edit box " + element.getAttribute("name"));
			Reporter.log("Pass");
		}
		catch(NoSuchElementException e)
		{
			//Click(element);
		}
		catch(StaleElementReferenceException seE)
		{
			//Click(element);
		}
		catch(NullPointerException npe)
		{
			//Click(element);
		}
	}
	
	
	public static String checkExit()
	{
		if(exitIter.contains("Fail"))
		{
			return exitIter;
		}
		return currFolder;
	}
	
	
	
	 public static void str_compare(String serval, String dbval, String ColName)
	   {
		  if(serval.equals("null"))
		   {
			   if(dbval == null || dbval.trim() == "")
			   {
				  // logs.log("######## " + ColName + " :- Value Matches. Value is " + serval);
				   htmlReports.reportPASS("######## " + ColName + " :- Value Matches. Value is " + serval);
			   }
			   else
			   {
				   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + dbval + " but the Serv val is " + serval); 
			   }
			   
		   }
		  else if(dbval == null)
		   {
			  htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + "null" + " but the Serv val is " + serval);  
		   }
			  
		  else if(serval.equals(dbval))
		   {
			   //System.out.println("values Matches. Value is " + serval);
			  htmlReports.reportPASS("######## " + ColName + " :- value Matches. Value is " + serval);
		   }
		   else
		   {
			  // System.out.println("DB value is " + dbval + " but the Serv val is " + serval);
			   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + dbval + " but the Serv val is " + serval);
		   }
		   

	   }
	 
	 
	   public static void str_contains(String fullString, String partString, String ColName)
	   {
		  if(fullString.equals("null"))
		   {
			   if(partString == null)
			   {
				   htmlReports.reportPASS("######## " + ColName + " :- values Matches. Value is " + fullString);
			   }
			   else
			   {
				   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + partString + " but the Serv val is " + fullString); 
			   }
			   
		   }
			  
		  else if(fullString.contains(partString))
		   {
			   //System.out.println("values Matches. Value is " + serval);
			  htmlReports.reportPASS("######## " + ColName + " :- values Matches. Value is " + fullString);
		   }
		   else
		   {
			  // System.out.println("DB value is " + dbval + " but the Serv val is " + serval);
			   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + partString + " but the Serv val is " + fullString);
		   }
		   

	   }
	   
	   
	   public static void Date_Compare(java.util.Date empdate, java.util.Date empdate_Pega, String ColName)
	   {
		  if(empdate.equals("null"))
		   {
			   if(empdate_Pega == null)
			   {
				   htmlReports.reportPASS("######## " + ColName + " :- values Matches. Value is " + empdate.toString());
			   }
			   else
			   {
				   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + empdate_Pega.toString() + " but the Serv val is " + empdate.toString()); 
			   }
			   
		   }
			  
		  else if(empdate.compareTo(empdate_Pega) == 0)
		   {
			   //System.out.println("values Matches. Value is " + serval);
			  htmlReports.reportPASS("######## " + ColName + " :- values Matches. Value is " + empdate.toString());
		   }
		   else
		   {
			  // System.out.println("DB value is " + dbval + " but the Serv val is " + serval);
			   htmlReports.reportFAIL("######## " + ColName + " :- FAIL - App value is " + empdate_Pega.toString() + " but the Serv val is " + empdate.toString());
		   }
		   

	   }
	   
	
}
