package utility;


import java.io.File;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
public class screenshot 

{

   @Test

   public void TakeScreenShot(WebDriver driver, String filepath) throws Exception{

      this.takeSnapShot(driver, filepath) ;  
      

   }         

   /** This function will take screenshot
	    * @param webdriver
	    * @param filepath	    
	    * @throws Exception
	    * @return 
    */

   public static WebDriver takeSnapShot(WebDriver webdriver,String filepath) throws Exception{

       //Convert web driver object to TakeScreenshot
      TakesScreenshot scrShot =((TakesScreenshot)webdriver);
	  // Screenshot scrShot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(webdriver);
       //Call getScreenshotAs method to create image file
      File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
	   //File SrcFile=scrShot.getImage().;
           //Move image file to new destination
      File DestFile=new File(filepath);
               //Copy file at destination
       FileUtils.copyFile(SrcFile, DestFile);
               
/*               Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(webdriver);
               ImageIO.write(screenshot.getImage(),"PNG",new File(filepath));
               htmlReports.addScreenshot(filepath);
               */
       takeFullScreenshot(webdriver, filepath);
               return webdriver;    
              

   }
   
   public static WebDriver takeFullScreenshot(WebDriver driver, String filepath) throws Exception
   {
	   Screenshot scrShot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(100)).takeScreenshot(driver);
	   ImageIO.write(scrShot.getImage(),"PNG",new File(filepath));
	   return driver;
   }
   


}