package Scenario;
import org.testng.annotations.Test;

import Page.PageLogin;
import Test.*;
import utility.Enums.*;
import utility.Enums;
import utility.logs;

import utility.dataBase_Utility;
import utility.htmlReports;

public class Sponsor extends BaseTest {
	
	public static void ApproveCase(String Caseno) throws Exception
		{
			logs.log("########################Start of Sponsor Approve Case##################################");
			String qry = "";
			Login.LoginasSponsorQA();
			sponsor.BeginCase(Caseno);
			String ApprovedTime = sponsor.approveCase();
			String lines[] = ApprovedTime.split("(\r\n|\r|\n)", -1);
			htmlReports.reportINFO("Approved Time : " + lines[1]);
			Login.Logout();
		}
}
