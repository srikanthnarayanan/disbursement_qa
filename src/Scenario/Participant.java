package Scenario;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import Page.PageLogin;
import Test.*;
import testDataFactory.testData;
import utility.Enums.*;
import utility.ReusableLibraries;
import utility.Enums;
import utility.logs;

import utility.dataBase_Utility;
import utility.htmlReports;

public class Participant extends BaseTest {
	
	public static String CreateCase(DistributionOption DistributionOption, SpousalConsent SpousalConsent,RMD RMD,WithDrawalType WithDrawalType,WithDrawalOption WithDrawalOption,TAXWH TAXWH,Reservist Reservist) throws Exception
		{
			logs.log("########################Start of Create Case##################################");
			String qry = "";
			if(SpousalConsent == Enums.SpousalConsent.Yes)
				{
					qry = "select A1.PRTCPNT_SSN, A2.PLAN_NBR from ods.PRTCPNT_REF A1 inner join ods.plan_ref A2 on(A1.plan_REF_GEN_ID = A2.plan_REF_GEN_ID) inner join ods.PLAN_SVC_VW A3 on(A2.Plan_NBR = A3.planNumber and DistributionSpousalConsentRequiredIndicator like 'Y' and plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY')";
				}
			else
				{
					qry = "select A1.PRTCPNT_SSN, A2.PLAN_NBR from ods.PRTCPNT_REF A1 inner join ods.plan_ref A2 on(A1.plan_REF_GEN_ID = A2.plan_REF_GEN_ID) inner join ods.PLAN_SVC_VW A3 on(A2.Plan_NBR = A3.planNumber and DistributionSpousalConsentRequiredIndicator like 'N' and plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY')";
				}
		//--------------------------------------------------------		
			if(RMD == Enums.RMD.Yes)
				{
					qry = qry + " intersect Select ParticipantNumber,PlanNumber from ods.PRTCPNT_PLAN_ATTR_VW where PlanNumber in(Select PlanNumber from ods.plan_ref where plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY') and MinimumDistributionSuppressIndicator like '0'";
				}
			else
				{
					qry = qry + " intersect Select ParticipantNumber,PlanNumber from ods.PRTCPNT_PLAN_ATTR_VW where PlanNumber in(Select PlanNumber from ods.plan_ref where plan_ADMIN_CNTCT_NAME like 'NADINE BRINDLEY') and MinimumDistributionSuppressIndicator like '1'";
				}
		//----------------------------------------------------------		
			if(WithDrawalType == Enums.WithDrawalType.Full)
				{
					
				}
			else
				{
				
				}
		//---------------------------------------------------------
			if(TAXWH == Enums.TAXWH.NA)
				{
					
				}
			else if(TAXWH == Enums.TAXWH.NO)
				{
					
				}
			else
				{
					
				}
		//------------------------------------------------------
			qry = qry + " intersect select ParticipantNumber, PLanNumber from ods.PRTCPNT_REF_NHOP_VW where ParticipantTerminationDate is not null intersect select ParticipantNumber, planNumber from ods.PRTCPNT_BAL_VW where VestedBalanceAmount > 0 order by PRTCPNT_SSN";
			
			String Participant_Nbr = dataBase_Utility.dbRetrieve(qry,"PRTCPNT_SSN");
			String Plan_Nbr = dataBase_Utility.dbRetrieve(qry,"PLAN_NBR");
			
			//String Participant_Nbr = "553761761";
			//String Plan_Nbr = "G38317";
			
			
			Login.LoginasQA();
			selectPlan.startMyDistribution();
			selectPlan.SelectPlanid(Participant_Nbr,Plan_Nbr);
			String ext = selectPlan.selectParLink();
			if(ext.contains("Fail"))
			{
				return ext;
			}
			if(DistributionOption == Enums.DistributionOption.SFS)
			{
				DistributionOptions.selectSFS();
			}
			else
			{
				DistributionOptions.selectIRAD();
			}
			
			ConfirmStatus.QDRO_No();
			if(SpousalConsent == Enums.SpousalConsent.Yes)
				{
					
					ConfirmStatus.RMD_Next();
					ConfirmStatus.Spousal_Download();
					ConfirmStatus.Spousal_Upload();	
				}
			
				AccountBalance.validate_Balances(Plan_Nbr, Participant_Nbr);
				AccountBalance.clickNext();
		//------------------------------------------------------------------------
				if(WithDrawalType == Enums.WithDrawalType.Full)
				{
					WithdrawalOptions.clickFull();
				}
			else
				{
				WithdrawalOptions.clickPartial();
				}
		//------------------------------------------------------------------------------------
				if(WithDrawalOption == Enums.WithDrawalOption.InternalRollOver)
				{
					WithdrawalOptions.selectRollOver();
					WithdrawalOptions.selectInternalRollOver();
				}
				else if(WithDrawalOption == Enums.WithDrawalOption.ExternalRollover)
				{
					WithdrawalOptions.selectRollOver();
					WithdrawalOptions.selectExternalRollOver();
				}
				else if(WithDrawalOption == Enums.WithDrawalOption.Check)
				{
					WithdrawalOptions.selectCash();
					WithdrawalOptions.selectCheck();
				}
				else
				{
					WithdrawalOptions.selectCash();
					WithdrawalOptions.selectACH();
				}
		//------------------------------------------------------------------------			
				if(TAXWH == Enums.TAXWH.NA)
				{
					TaxElections.TaxElection_NA();
				}
			else if(TAXWH == Enums.TAXWH.NO)
				{
					TaxElections.TaxElection_No();
				}
			else
				{
					TaxElections.TaxElection_Yes();
				}
		//-----------------------------------------------------------------------------
				if(Reservist == Enums.Reservist.Yes)
				{
					Reservists.Reservist_Yes();
				}
				else
				{
					Reservists.Reservist_No();
				}
				
				Summary.Summary_Next();
				Summary.TermsConditions();
				String Caseno = Summary.getCaseNo();
				Login.Logout();
				return Caseno;
		}


	
	public static String CreateCase_Excel(int i) throws Exception
	{
		logs.log("########################Start of Create Case##################################");
		String qry = "";
		String Participant_Nbr,Plan_Nbr,DistributionOption,WithDrawalType,WithDrawalOption,TAXWH,Reservist;
		String Caseno;


				Participant_Nbr = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "Participant");
				Plan_Nbr =  testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "Plan");
				DistributionOption = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "DistributionOption");
				WithDrawalType = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "WithDrawalType");
				WithDrawalOption = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "WithDrawalOption");
				TAXWH = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "TAXWH");
				Reservist = testData.getstringcellvalue1("CreateCase", "CreateCase", ""+i, "Reservist");
				
				Login.LoginasQA();
				//Login.switch_application();
				
				selectPlan.startMyDistribution();
				selectPlan.SelectPlanid(Participant_Nbr,Plan_Nbr);
				
				exitIter = selectPlan.selectParLink();
//------------------------------------------------				
				if(exitIter.contains("Fail"))
				{
					return exitIter;
				}
//------------------------------------------------
				
				if(DistributionOption.equalsIgnoreCase("SFS"))
				{
					DistributionOptions.selectSFS();
				}
				else
				{
					DistributionOptions.selectIRAD();
				}
				ConfirmStatus.QDRO_Explanation();
				ConfirmStatus.QDRO_No();
				ConfirmStatus.RMD_Next();
				ConfirmStatus.Spousal_Download();
				ConfirmStatus.Spousal_Upload();	
				
				//AccountBalance.specialTaxNotice(); // need to work on how to identify if the file has been downloaded successfully
				AccountBalance.validate_Balances(Plan_Nbr, Participant_Nbr);
				AccountBalance.clickNext();
			//------------------------------------------------------------------------
					if(WithDrawalType.equalsIgnoreCase("Full"))
					{
						WithdrawalOptions.clickFull();
					}
					else
					{
						WithdrawalOptions.clickPartial();
					}
			//------------------------------------------------------------------------------------
					if(WithDrawalOption.equalsIgnoreCase("InternalRollOver"))
					{
						WithdrawalOptions.selectRollOver();
						WithdrawalOptions.selectInternalRollOver();
					}
					else if(WithDrawalOption.equalsIgnoreCase("ExternalRollover"))
					{
						WithdrawalOptions.selectRollOver();
						WithdrawalOptions.selectExternalRollOver();
					}
					else if(WithDrawalOption.equalsIgnoreCase("Check"))
					{
						WithdrawalOptions.selectCash();
						WithdrawalOptions.selectCheck();
					}
					else
					{
						WithdrawalOptions.selectCash();
						WithdrawalOptions.selectACH();
					}
			//------------------------------------------------------------------------			
					if(TAXWH.equalsIgnoreCase("NA"))
					{
						TaxElections.TaxElection_NA();
					}
				else if(TAXWH.equalsIgnoreCase("NO"))
					{
						TaxElections.TaxElection_No();
					}
				else
					{
						TaxElections.TaxElection_Yes();
					}
			//-----------------------------------------------------------------------------
					if(Reservist.equalsIgnoreCase("Yes"))
					{
						Reservists.Reservist_Yes();
					}
					else
					{
						Reservists.Reservist_No();
					}
					Summary.verifyDataSummary(Plan_Nbr, Participant_Nbr);
					Summary.Summary_Next();
					Summary.TermsConditions();
					Caseno = Summary.getCaseNo();
					Login.Logout();
					return Caseno;

	}
	
	
	
	public static String waituntil_PendingDivertDecision(String CaseNo) throws Exception
	{
		Login.LoginasQA();
		String Status = "";
		int i = 0;
		do
		{
			
			i = i + 1;
			logs.log("&&&&&&&&&&&&In iteration " + i + "******************");
			ReusableLibraries.switchDefaultFrame();
			WebElement Dashboard = driver.findElement(By.xpath("//li[@title='Dashboard']"));
			//Dashboard.click();
			ReusableLibraries.Click(Dashboard);
			
			SearchCase(CaseNo);
			//ReusableLibraries.sync(High_Wait);
			
			
			
			Thread.sleep(50000);
			ReusableLibraries.switchFrame();
			
			WebElement caseStatus = driver.findElement(By.xpath("//*[@data-test-id='2016083016191602341167946']"));
			if(caseStatus.getAttribute("innerHTML").equalsIgnoreCase("Pending-Divert Decision"))
			{
				if(i>6)
				{
					Status =  "Fail - Case doesn't move out of Pending-Divert Decision";
					break;
				}
			}
			else
			{
				Status =  caseStatus.getAttribute("innerHTML");
				break;
			}
			
		//		List <WebElement> allSpans =  driver.findElements(By.tagName("span"));
		//		for(WebElement span : allSpans)
		//		{
		//			if(span.getAttribute("innerHTML").equalsIgnoreCase("Pending-Divert Decision"))
		//			{
		//				Status = "Pending-Divert Decision";
		//				logs.log("STatus is :" + Status);
		//				break;
		//			}
		//		}
		//		if(i==30)
		//		{
		//			logs.log("Case status doesn't change and still in Pending-Divert Decision");
		//			return "Fail";
		//		}
		}
		while(1==1);
		//while(Status == "Pending-Divert Decision" || (i<30)); 
		
		
		return Status;
	}

    
	public static void CompleteTasks() throws Exception
	{
		htmlReports.reportINFO("------- Searching for Case and Begin----");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		
		//-------------gettransactions is called here----------
		getTransactions();
		
		int noofRows = driver.findElements(By.xpath("/html/body/div[2]/form/div[3]/div/section/div/span[3]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div/div/div/div[5]/div[2]/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr/td[2]/div/table/tbody/tr")).size();
		noofRows = noofRows - 1;
		for(int i = 1;i<=noofRows;i++)
		{
			//-----------getting Divert reason from the Application
			String instructions = driver.findElement(By.xpath("/html/body/div[2]/form/div[3]/div/section/div/span[3]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div/div/div/div/div/div/div[5]/div[2]/div/div/div/div/div/div/div/div[2]/div/div[2]/div/div/div[1]/table/tbody/tr/td[2]/div/table/tbody/tr[" + (i+1) + "]/td[7]/div/span")).getAttribute("innerHTML");
			htmlReports.reportINFO("Divert Reason is " + instructions);
			
			//------------Getting reason from TestData Sheet
			String Reason = testData.getstringcellvalue("CreateCase","CreateCase","Iteration","1","ValidateDivertReason");

			//-----------If there are multiple reasons and if this reason matches with any one it throws pass else fail
			if(Reason.contains(instructions))
			{
				htmlReports.reportPASS("Divert Reason matches as expected. Reason is " + instructions );
			}
			else
			{
				htmlReports.reportFAIL("Divert Reason doesn't exist as expected. Expected is " + Reason + " Actual is " + instructions);
			}

			//----------changing the Open dropdown to Completed dropdown
			new Select(driver.findElement(By.id("Status"+i))).selectByVisibleText("Completed");
		}
		htmlReports.addScreenshot(currFolder +"\\StatusComplete" + currIteration + ".jpg" );
		
		//------------clicking on submit button once done----
		ReusableLibraries.Click("xpath", "//*[@class='field-item dataValueRead']//button[@class='Strong pzhc pzbutton']");
		
		ReusableLibraries.switchFrame();

		//-------------Fetching the case status and returning
		WebElement caseStatus = driver.findElement(By.xpath("//*[@class='content-item content-field item-3 remove-bottom-spacing remove-right-spacing flex flex-row badge-bg-wait centered dataValueRead']//*[@data-test-id='2016083016191602341167946']"));
		htmlReports.reportINFO(caseStatus.getAttribute("innerHTML"));
		htmlReports.addScreenshot(currFolder +"\\PendingOmni" + currIteration + ".jpg" );
	}
	
	
	public static void getTransactions() throws Exception
	{
		//-------------gettransactions is called at CompleteTasks----------
		htmlReports.reportINFO("------- Get Transactions from Case----");
		//String basexpath = "/html/body/div[3]/form/div[3]/div[2]/section/div/span[3]/div/div/div/div/div[1]/div/div/div/div/div/div/div/div[2]/div/div/div/div/div[5]/div[2]/div/div/div/div/span/div/div/div/span/div/div/div[2]/div/div/div/table/tbody/tr/td/div/div/div[1]/table/tbody/tr/td[2]/div/table/tbody/tr";
		int i = 0;
		
		//-----performs the do loop until a row isn't available		
		do
		{
			i = i + 1;
			 if(driver.findElements(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']")).size() > 0)
			 {
				String FileId = driver.findElement(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']/td[1]/div/span")).getAttribute("innerHTML");
				String TransactionStatus = driver.findElement(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']/td[2]/div/span")).getAttribute("innerHTML");
				String UserId = driver.findElement(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']/td[3]/div/span")).getAttribute("innerHTML");
				String TradeDate = driver.findElement(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']/td[4]/div/span")).getAttribute("innerHTML");
				String SeqNumber = driver.findElement(By.xpath("//tr[@id='$PpyWorkPage$pTransactionList$l" + i + "']/td[5]/div/span")).getAttribute("innerHTML");
				
				htmlReports.reportINFO("######## FileId is " + FileId);
				htmlReports.reportINFO("######## TransactionStatus is " + TransactionStatus);
				htmlReports.reportINFO("######## UserId is " + UserId);
				htmlReports.reportINFO("######## TradeDate is " + TradeDate);
				htmlReports.reportINFO("######## SeqNumber is " + SeqNumber);
			 }
			 else
			 {
				 break;
			 }
		}while(1==1);
		
	}
	
	public static void SearchCase(String CaseNo) throws Exception
	{
		htmlReports.reportINFO("------- Searching for Case and Begin----");
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchDefaultFrame();
		
		ReusableLibraries.sync(High_Wait);
		
		//-------------enters the case number and clicks enter in the search box
		driver.findElement(By.id("pySearchText")).clear();
		ReusableLibraries.Set("id", "pySearchText", CaseNo + Keys.ENTER);
		Thread.sleep(3000);
		try 
		{
			driver.switchTo().alert().accept();
			logs.log("Alert closed");
		}
		catch(NoAlertPresentException e) 
		{
			
		}
		catch(UnhandledAlertException ue)
		{
			
		}
		
		ReusableLibraries.sync(High_Wait);
		ReusableLibraries.switchFrame();
		htmlReports.addScreenshot(currFolder +"\\Sponsor_SearchCase" + currIteration + ".jpg" );

	}


}
